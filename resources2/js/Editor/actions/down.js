GDE_EditorActions.prototype.down = function(context, command) { 	
    event.stopPropagation();
    if(context.selection.area != null){
        context.selection.clearSelection(context);
        return;
    }
    
    if(context.height < context.getInnerHeight() && context.currentLine + 1 >= context.rendered.length - 1){ 
        return; 
    }

    if(context.currentLine + 1 < context.totalLines){
        if(context.position.stored == null){
            context.storeCurrentPosition();
        }

        var previousLine = context.getCurrentLine().substring(context.position.relative, context.getCurrentLine().length).length;
        context.currentLine++;
        

        if(context.position.relative > context.getCurrentLine().length){						
            context.addToPosition({ absolute : 
                (
                    previousLine +  
                    context.getCurrentLine().length + 
                    3
                )
            });
            context.setPosition({ relative : context.getCurrentLine().length });
        } else {
            context.addToPosition({ absolute : 
                (
                    previousLine + 
                    context.position.relative +
                    3
                ) 
            });
            context.setPosition({ relative : context.position.relative});
        }

        context.adjustForStoredPosition();
    

        context.outputAdaptor.cursorElement.classList.add('static');
        context.updateOutput.call(context);
        context.outputAdaptor.cursorElement.classList.remove('static');

        context.refreshPoint();
    }
};
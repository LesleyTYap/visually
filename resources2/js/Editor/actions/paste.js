GDE_EditorActions.prototype.paste = function(context, command) { 		
    event.stopPropagation();
    if(context.hasSelection()){
        context.removeSelection();
        context.clearSelected();
    }
    
    var promise = context.utility.paste();
    promise.then((clipboard) => {
        clipboard = context.utility.processInput(clipboard);

        for(var i = 0; i < clipboard.length; i++){
            context.processDefault({key : clipboard[i]});
        }

        context.selection.clearSelection(context);
        context.updateOutput.call(context);
        context.refreshPoint();
    });

};
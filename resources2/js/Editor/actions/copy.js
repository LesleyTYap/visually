GDE_EditorActions.prototype.copy = function(context, command) { 			
    event.stopPropagation();
    var selection = context.selection.getSelection(context);
    var content = context.content.substring(selection.start.position.absolute, selection.end.position.absolute);
    content = context.utility.processOutput(content);
    var promise = context.utility.copy(content);

    context.updateOutput.call(context);
    context.refreshPoint();
};
/* MAKE TEXT NOT SELECTABLE */
/* DISAPLY BACKGROUND BEHIND SELECTED AREA */

function Selection(){
    
    this.editor;
    this.point;
    this.selection;

    this.area;

    this.isSelecting;
    this.isDragging;
	
	this.init = function(){
        this.selection = {};
        this.area = null;
        this.isSelecting = false;

		return this;
	};
	if (!(this instanceof Selection)) { throw('Constructor called without "new"'); }
	return this.init.apply(this, []);
}

Selection.prototype.getPoint = function(editor){
    editor.clearCurrentPosition();
    var previous = { line : null, position : null };
    
    if(typeof event.clientX != 'undefined'){
        var position = editor.outputAdaptor.getElementPosition(editor);
        var left = event.clientX -position.left  + editor.padding.left;
        var top = event.clientY - position.top + editor.padding.top; 
        var line = Math.round(top / (parseInt(editor.fontSize) * editor.lineHeight));

        if(line < 0){ line = 0; }
        else if(line > editor.rendered.length - 1){ return; }
        else if(editor.height < editor.getInnerHeight() && line >= editor.rendered.length - 1){ return; }
        
        previous.position = editor.position.relative;
        previous.line = editor.currentLine;
        
        editor.currentLine = Math.abs(line);
        if((left - editor.padding.left) < 0 && (left - editor.padding.left) >= -8){ editor.position.relative = 0; }
        else if(left < -8){ return; }
        else {
            var text = editor.utility.getSubStringByWidth(
                editor,
                editor.getCurrentLine(),
                left
            );

            editor.position.relative = (left < editor.utility.getStringWidth(text)) ? text.length - 1 : text.length;
        }
    } else {
        previous.position = editor.position.relative;
        previous.line = editor.currentLine;
    }   
    
	var between = editor.utility.getTextBetween(editor, previous.line, editor.currentLine, previous.position, editor.position.relative);
	var adjust = between.length + Math.abs(previous.line - editor.currentLine) * 3;

	if(previous.line < editor.currentLine || (previous.line == editor.currentLine && previous.position < editor.position.relative)){
        editor.addToPosition({ absolute : adjust });
	} else {
        editor.subtractFromPosition({ absolute : adjust });
    }
    
    this.point = { 
        line : editor.currentLine, 
        position : {
            relative : editor.position.relative,
            absolute : editor.position.absolute,
            stored : editor.position.stored
        }
    };
};

Selection.prototype.select = function(){
    this.isSelecting = true;
};

Selection.prototype.blur = function(){
    this.isSelecting = false;
    this.isDragging = false;
};

Selection.prototype.fetchSelection = function(editor, fetchPoint){
    if(typeof fetchPoint == 'undefined'){
        fetchPoint = false;
    }
    if(typeof this.point != 'undefined'){
        if(this.point == null){
            this.point = this.getPoint(editor);
        }

        
        var point = this.point;
        this.getPoint(editor);
        var start, end;

        if(this.point.position.absolute < point.position.absolute){
            start = this.point;
            end = point;
        } else {
            start = point;
            end = this.point;
        }

        this.selection =  {
            start : start,
            end : end,
            origin : point
        };

        this.getSelectedArea(editor);
        editor.outputAdaptor.selected(editor, this.area);
        editor.updateCursor();
        
        if(fetchPoint === false){
            this.point = point;
        }

        return this.selection;
    }
}

Selection.prototype.hasSelection = function(){
    return (this.selection !== null && typeof this.selection.origin != 'undefined');
};

Selection.prototype.getSelection = function(editor){
    if(this.selection == null){
        this.selection = this.fetchSelection(editor);
    }
    return this.selection;
}

Selection.prototype.clearSelection = function(editor){
    if(typeof editor != 'undefined'){
       editor.clearSelected(); 
    }
    this.area = null;
    this.point = null;
    this.selection = null;
};

Selection.prototype.getSelectedArea = function(editor){
    var selection = this.getSelection();
    var areas = {}, start, offsetStart, offsetEnd;
    for(var i = selection.start.line; i < selection.end.line + 1; i++){
        if(i === selection.start.line && i === selection.end.line){
            offsetStart = 0;
            start = selection.start.position.relative;
            offsetEnd = selection.end.position.relative;
        } else if(i === selection.start.line){
            offsetStart = 0;
            start = selection.start.position.relative;
            offsetEnd = editor.rendered[i].length;
        } else if(i === selection.end.line){
            offsetStart = 0;
            start = 0;
            offsetEnd = selection.end.position.relative;
        } else {
            offsetStart = 0;
            start = 0;
            offsetEnd = editor.rendered[i].length;
        }

        areas[i] = {
            offset : editor.utility.getStringWidth(
                editor, 
                editor.rendered[i].substring(offsetStart, start)
            ),
            width : editor.utility.getStringWidth(
                editor, 
                editor.rendered[i].substring(start, offsetEnd)
            )
        };
    }
    this.area = areas;
};

Selection.prototype.getWordAtPosition = function(content, position) {
    const isSpace = (c) => /\s/.exec(c);
    let start = position - 1;
    let end = position;
  
    while (start >= 0 && !isSpace(content[start])) {
      start -= 1;
    }
    start = Math.max(0, start + 1);
  
    while (end < content.length && !isSpace(content[end])) {
      end += 1;
    }
    end = Math.max(start, end);
  
    return { start : start, end : end };
};

Selection.prototype.selectWord = function(editor){
    var line = editor.getCurrentLine();
    var bounds = this.getWordAtPosition(line, editor.position.relative);
    var start = {
        line : editor.currentLine, 
        position : {
            relative : bounds.start,
            absolute : editor.position.absolute - (editor.position.relative - bounds.start),
            stored : null
        }
    };

    var end = {
        line : editor.currentLine, 
        position : {
            relative : bounds.end,
            absolute : editor.position.absolute + (bounds.end - editor.position.relative),
            stored : null
        }  
    };
    this.point = start;
    this.selection =  {
        start : start,
        end : end,
        origin : start
    };
    
    
    editor.setPosition({
        relative : end.position.relative,
        absolute : end.position.absolute
    });
    
    this.getSelectedArea(editor);
    editor.outputAdaptor.selected(editor, this.area);
    editor.updateCursor();
};

Selection.prototype.selectSentence = function(editor){
    var line = editor.getCurrentLine();

    var start = {
        line : editor.currentLine, 
        position : {
            relative : 0,
            absolute : editor.position.absolute - editor.position.relative,
            stored : null
        }
    };

    var end = {
        line : editor.currentLine, 
        position : {
            relative : line.length,
            absolute : editor.position.absolute + (line.length - editor.position.relative),
            stored : null
        }  
    };
    this.point = start;
    this.selection =  {
        start : start,
        end : end,
        origin : start
    };

    editor.setPosition({
        relative : end.position.relative,
        absolute : end.position.absolute
    });

    console.log('teast');
    this.getSelectedArea(editor);
    editor.outputAdaptor.selected(editor, this.area);
    editor.updateCursor();
};

Selection.prototype.selectAll = function(editor){
    var start = {
        line : 0, 
        position : {
            relative : 0,
            absolute : 0,
            stored : null
        }
    };

    var end = {
        line : editor.rendered.length - 1, 
        position : {
            relative : editor.rendered[editor.rendered.length - 1].length,
            absolute : editor.content.length,
            stored : null
        }  
    };
    this.point = start;
    this.selection =  {
        start : start,
        end : end,
        origin : start
    }

    editor.currentLine = end.line;
    editor.setPosition({
        relative : end.position.relative,
        absolute : end.position.absolute
    });

    this.getSelectedArea(editor);
    editor.outputAdaptor.selected(editor, this.area);
    editor.updateCursor();
};
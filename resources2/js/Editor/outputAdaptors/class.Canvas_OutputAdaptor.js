function Canvas_OutputAdaptor(element){	
    
    this.element;
    this.cursorElement;
    this.selectedElement;
    this.elementStyles;

    this.canvasElement;
    
    this.init = function(element){    
        this.canvasElement = element;

        this.element = this.canvasElement.canvas.element;
        this.elementStyles = window.getComputedStyle(this.element);
		return this;
    };
    
    this.process = function(editor){
        visually.draw();        
    };

    this.moveCursor = function(editor, line, x){
        if(typeof this.cursorElement == 'undefined'){
            this.cursorElement = document.createElement('span');
            this.cursorElement.classList.add('gde-cursor');
            
            this.cursorElement.style.height = (parseInt(editor.fontSize) * 0.8) + 'px';
            this.cursorElement.style.marginTop = (parseInt(editor.fontSize) * 0.2) + 'px';
            this.cursorElement.style.top = '0px';
            this.cursorElement.style.left = '0px';
            
            this.element.parentNode.appendChild(this.cursorElement);
        }
        if(!editor.isActive()){ 
            this.cursorElement.style.display = 'none';
            return; 
        }
        this.cursorElement.style.display = 'block';

        var xAdjustment;
        if(editor.textAlign == 'center'){
            xAdjustment = parseInt(this.elementStyles.getPropertyValue('padding-left')) + this.canvasElement.elementPosition.x + this.canvasElement.canvasOffset.left + (this.canvasElement.getWidth() * 0.5);
            xAdjustment -= x;
            xAdjustment += x * 0.5;
        } else if(editor.textAlign == 'right'){
            xAdjustment = parseInt(this.elementStyles.getPropertyValue('padding-left')) + this.canvasElement.elementPosition.x + this.canvasElement.canvasOffset.left - editor.padding.right + this.canvasElement.getWidth();
            xAdjustment -= x;
        } else {
            xAdjustment = parseInt(this.elementStyles.getPropertyValue('padding-left')) + this.canvasElement.elementPosition.x + this.canvasElement.canvasOffset.left + editor.padding.left;
        }
        
        this.cursorElement.style.left = x +  xAdjustment + 'px';
        this.cursorElement.style.top = (this.canvasElement.elementPosition.y + (parseInt(editor.fontSize) * (editor.lineHeight * (line)))) - parseInt(editor.fontSize) + 'px';
    };

    this.selected = function(editor, areas){
        var span;
        if(typeof this.selectedElement == 'undefined'){
            this.selectedElement = document.createElement('div');
            this.selectedElement.classList.add('gde-selected');
            this.element.parentNode.appendChild(this.selectedElement);
        }
        this.selectedElement.innerHTML = '';

        var position;
        if(typeof editor != 'undefined' && typeof areas != 'undefined'){
            position = this.getElementPosition(editor);
        }

        
        for(var index in areas){
            if(areas.hasOwnProperty(index)){
                var xAdjustment;
                if(editor.textAlign == 'center'){
                    xAdjustment = parseInt(this.elementStyles.getPropertyValue('padding-left')) + this.canvasElement.elementPosition.x + this.canvasElement.canvasOffset.left + (this.canvasElement.getWidth() * 0.5);
                    xAdjustment -= areas[index].width;
                    xAdjustment += areas[index].width * 0.5;
                } else if(editor.textAlign == 'right'){
                    xAdjustment = parseInt(this.elementStyles.getPropertyValue('padding-left')) + this.canvasElement.elementPosition.x + this.canvasElement.canvasOffset.left - editor.padding.right + this.canvasElement.getWidth();
                    xAdjustment -= areas[index].width;
                } else {
                    xAdjustment = parseInt(this.elementStyles.getPropertyValue('padding-left')) + this.canvasElement.elementPosition.x + this.canvasElement.canvasOffset.left + editor.padding.left;
                }
                                
                span = document.createElement('span');
                span.style.left = areas[index].offset + xAdjustment + 'px';
                span.style.top = (this.canvasElement.elementPosition.y + (parseInt(editor.fontSize) * (editor.lineHeight * (index)))) - parseInt(editor.fontSize) + 'px';
                span.style.marginTop = (parseInt(editor.fontSize) * 0.2) + 'px';
                span.style.width = areas[index].width + 'px';
                    
                setTimeout(function(span, editor){
                    span.addEventListener('click', this.removeSelection.bind(editor), true);
                }.bind(this, span, editor), 200);
                this.selectedElement.appendChild(span);
            }
        }
    }

    this.removeSelection = function(){
        if(this.content.length > 0){
            this.selection.clearSelection(this);
            this.selection.select();
            this.selection.getPoint(this);
            this.updateOutput.call(this);            
        }
    }

    this.getElementPosition = function(editor){
        var rect = editor.outputAdaptor.element.getBoundingClientRect();
        return { 
            left : rect.left + this.canvasElement.elementPosition.x,
            top : rect.top + this.canvasElement.elementPosition.y
        };
    }


	if (!(this instanceof Canvas_OutputAdaptor)) { throw('Constructor called without "new"'); }
	return this.init.apply(this, [element]);
}
function GDE_EditorUtilities(){

    this.textTest;

	this.init = function(){
        this.textTest = null;
        
		return this;
	};
	if (!(this instanceof GDE_EditorUtilities)) { throw('Constructor called without "new"'); }
	return this.init.apply(this, []);
}

GDE_EditorUtilities.prototype.fitTextIntoEditor = function(editor){
    var previousLines = editor.rendered.length;
    
    var result = this.logarithmicMerge(editor);
    editor.content = result.join('');
    editor.rendered = editor.content.split(/(?:%f%|%n%)/g);
    editor.totalLines = editor.rendered.length;
};

GDE_EditorUtilities.prototype.getSubStringByWidth = function(editor, text, width){
    var subString;
    var run = true;
    var edge = { left: 0, right: text.length };
        
    var trail = edge.right.text;
    while(run) {
        subString = text.substring(0, edge.right);
        if(this.testTextWidth(editor, subString, width)){
            right = edge.right;
            edge.right += Math.ceil((subString.length - edge.left) / 2);
            edge.left = right;
        } else {
            edge.right -= Math.ceil((subString.length - edge.left) / 2);
        }
        if(edge.left === edge.right){
            run = false;
        }
    }
    return subString;
}

GDE_EditorUtilities.prototype.testTextWidth = function(editor, text, width){
    var testWidth = (typeof width != 'undefined') ? width : editor.width - editor.padding.left - editor.padding.right;
    if(typeof testWidth == 'undefined' || testWidth == null){ return true; }
    if(this.textTest ===  null){
        this.textTest = document.createElement('span');
        this.textTest.style.whiteSpace = 'pre';
        this.textTest.innerHTML = 'M';
    }
    
    if(typeof text != 'undefined'){
        this.textTest.innerHTML = text + 'M';
    }
    this.textTest.style.fontSize = editor.fontSize;
    this.textTest.style.fontFamily = editor.font;
    
    document.body.appendChild(this.textTest);
    var rect = this.textTest.getBoundingClientRect();
    this.textTest.remove();
           
    this.textTest.innerHTML = 'M';

    if(typeof width != 'undefined'){
        return testWidth > Math.round(rect.width);
    }
    return (typeof text !== 'undefined') ? testWidth >= rect.width : testWidth / rect.width;
};

GDE_EditorUtilities.prototype.getStringWidth = function(editor, text){
    if(this.textTest ===  null){
        this.textTest = document.createElement('span');
        this.textTest.style.display = 'inline-block';
        this.textTest.style.whiteSpace = 'pre';
        this.textTest.innerHTML = 'M';
    }
    
    var isSpace = false;// ((text.match(/( $||^ )/g) || []).length > 0) ? true : false;

    if(typeof text != 'undefined'){
        this.textTest.innerHTML = (isSpace) ? 'G' + text + 'D' : text;
    }
    this.textTest.style.fontSize = editor.fontSize;
    this.textTest.style.fontFamily = editor.font;
    
    document.body.appendChild(this.textTest);

    var width =  this.textTest.getBoundingClientRect().width;
    if(isSpace){
        this.textTest.innerHTML = 'GD';
        width -= this.textTest.getBoundingClientRect().width;
    }

    this.textTest.remove();

    return width;
};

GDE_EditorUtilities.prototype.getTextBetween = function(editor, a, b, aPosition, bPosition){
    var start, end, startPosition, endPosition;
    if(a <= b){
        start = a;
        startPosition = aPosition;
        end = b;
        endPosition = bPosition;
    } else {
        start = b;
        startPosition = bPosition;
        end = a;
        endPosition = aPosition;
    }
    var sliced = editor.rendered.slice(start, end + 1);
    var text = sliced.join('');

    if(typeof aPosition == 'undefined' || typeof bPosition == 'undefined'){
        return text;
    } else {
        return text.substring(
            startPosition, 
            text.length - sliced[sliced.length - 1].substring(endPosition).length
        );
    }
};

GDE_EditorUtilities.prototype.logarithmicMerge = function(editor){
    var text = editor.content;
    text = text.replace(/(?:%n%)/g, '');
    
    var edge, right, run;
    var addNewLine, reverted;
    var subString;
    var arr = [];
    
    var temp = text.split('%f%');

    for(var i = 0; i < temp.length; i++){
        run = true;
        addNewLine = false;
        reverted = false;
        edge = { left: 0, right: temp[i].length };
        
        while(run) {
            subString = temp[i].substring(0, edge.right);
            if(this.testTextWidth(editor, subString)){
                right = edge.right;
                edge.right += Math.ceil((subString.length - edge.left) / 2);
                edge.left = right;
                addNewLine = false;
            } else {
                edge.right -= Math.ceil((subString.length - edge.left) / 2);
                addNewLine = true;
            }

            if(edge.left === edge.right || edge.right < edge.left){
                run = false;
                subString += (addNewLine) ? '%n%' : '';
            }            
        }

        arr.push(subString);
        temp[i] = temp[i].substring(edge.right + 1, temp[i].length);
        if(temp[i].length > 0){ 
            reverted = true;
            i--; 
        }
        if(reverted !== true){ arr.push('%f%'); }
    }

    delete arr[arr.length -1];
    editor.content = arr.join('');
    return arr;
};

GDE_EditorUtilities.prototype.copy = async function(string){
    return await navigator.clipboard.writeText(string);
};

GDE_EditorUtilities.prototype.paste = async function(){
    return await navigator.clipboard.readText();
};

GDE_EditorUtilities.prototype.processInput = function(content){
    content = content.replace(/(?:\n)/g, '%f%');
    return content;
};

GDE_EditorUtilities.prototype.processOutput = function(content){
    content = content.replace(/(?:%f%)/g, '\n');
    content = content.replace(/(?:%n%)/g, '\n');
    return content;
};

function inherits(Child, Parent) {
	var Tmp_ = function () { };
	Tmp_.prototype = Parent.prototype;
	Child.prototype = new Tmp_();
	Child.prototype.constructor = Child;
}

if (typeof Event !== 'function') {
	function Event(type) { return { 'type': type }; }
}

var changeEvent = new Event('change');
var clickEvent = new Event('click');
var scrollEvent = new Event('scroll');
var resizeEvent = new Event('resize');

function triggerEvent(element, event) {
	if (typeof event === 'function') {
		element.dispatchEvent(new event.constructor(event.type, event));
	} else {
		if (document.fireEvent) {
			element.fireEvent(event.type);
		} else {
			var _event = document.createEvent("HTMLEvents");
			_event.initEvent(event.type, true, false);
			element.dispatchEvent(_event);
		}
	}
}
function GDE_ElementBuilder() {
	this.init = function(){ return this; };

	if (!(this instanceof GDE_ElementBuilder)) { throw('Constructor called without "new"'); }
	return this.init.apply(this);
}

GDE_ElementBuilder.prototype.build = function(elementConfig){
	if(typeof elementConfig === 'undefined'){ return false; }
	if(elementConfig === null){ return false; }
	if(typeof elementConfig.type !== 'string'){ return false; }

	let element = document.createElement(elementConfig.type);
	if(this.isValidElement(element)){
		if( typeof elementConfig.options !== 'undefined'){
			for(let index in elementConfig.options){
				if(index in element){
					element[index] = elementConfig.options[index];
				}
			}
		}
		return element;
	}
	return false;
};

GDE_ElementBuilder.prototype.isValidElement = function(element){
	return element.toString() != "[object HTMLUnknownElement]";
};
function GDE_FileLoader(){
    this.reader = new FileReader();
    
    this.init = function(){ return this; };
    
    if (!(this instanceof GDE_FileLoader)) { throw('Constructor called without "new"'); }
    return this.init.apply(this);
}

GDE_FileLoader.prototype.importImage = function(e, callback){
    this.reader.onload = function(file){
        callback(file.target.result);
    }
    this.reader.readAsDataURL(e.target.files[0]);  
}

GDE_FileLoader.prototype.loadImage = function(src, callback){
    this.reader.onload = function(file){
        callback(file.target.result);
    }
    this.reader.readAsDataURL(e.target.files[0]);  
}


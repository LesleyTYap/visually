function GDE_Cursor(options){
	
    this.position;
    this.callback;

	this.init = function(options){
        this.position = { x: 0, y: 0 };

        if(typeof options.callback != 'undefined'){
            this.callback = options.callback;
        }

		window.addEventListener('mousemove', this.follow.bind(this), false);
		return this;
	};

	if (!(this instanceof GDE_Cursor)) { throw('Constructor called without "new"'); }
	return this.init.apply(this, [options]);
}

GDE_Cursor.prototype.follow = function(e){
    this.position.x = e.clientX;
    this.position.y = e.clientY;
    if(typeof this.callback === 'function'){
        this.callback();
    }
};

GDE_Cursor.prototype.getX = function(){
    return this.position.x;
};

GDE_Cursor.prototype.getY = function(){
    return this.position.y;
};

GDE_Cursor.prototype.determinePointer = function(elements){
	let sniffed, cursor = { index : null, order : null, cursor : null };
	for(let index = 0; index < elements.length; index++){
		sniffed = elements[index].cursorBehaviour(this.position);
		if(sniffed !== false && 
			(cursor.cursor === null || cursor.order < elements[index].getOrder())){
			canMouseX = parseInt(this.getX() - elements[index].canvasOffset.left);
			canMouseY = parseInt(this.getY() - elements[index].canvasOffset.top);
			if(elements[index].isDraggable(canMouseX, canMouseY) || elements[index].canResize(canMouseX, canMouseY)){
				if(elements[index].isSelectable()){
					cursor = { index : index, order : elements[index].getOrder(), cursor : sniffed };
				} else {
					cursor = { index : index, order : elements[index].getOrder(), cursor : 'pointer' };
				}
			}
		}
	}

	return (cursor.cursor === null) ? 'pointer' : cursor.cursor;
}

GDE_Cursor.prototype.determineTarget = function(context, elements, event){
    let target = { index : null, order : -1 };
	for(let index in elements){
        if(elements[index].hasEvent(event.type)){
            canMouseX = parseInt(event.clientX - elements[index].canvasOffset.left);
            canMouseY = parseInt(event.clientY - elements[index].canvasOffset.top);
			if(elements[index].isTargetable(canMouseX, canMouseY)){
                if(target.index === null || target.order <= elements[index].getOrder()){
		 			target = { index : index, order : elements[index].getOrder() };
		 		}
		 	}
		}
	}	
	
	if(target.index === null){
		for(let index in elements){
			if(elements[index].state == 1 || elements[index].state === 2){
				target = { index : index, order : elements[index].getOrder() };
			}
		}
	}
	
	if(target.index !== null){
		for(index in elements){
			if(index != target.index){
				if(elements[index].state == 1 || elements[index].state === 2){
					elements[index].state = 0;
					elements[index].clearActive();
					context.draw();
				}
			}
        }
        return elements[target.index];
	} return false;
};
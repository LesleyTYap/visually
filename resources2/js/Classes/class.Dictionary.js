
function Dictionary(){
    this.keyValues = {};
    this.keyMap = null;

	this.init = function(){	
		return this;
    };
    
	if (!(this instanceof Dictionary)) { throw('Constructor called without "new"'); }
	return this.init.apply(this, []);
}

Dictionary.prototype.add = function(key, value){
	if(typeof this.getValue(key) == 'undefined' && typeof this.getKey(value) == 'undefined'){
        this.keyValues[key] = value;
        this.keyMap = null;
        return true;
    } else {
        console.error("Dictionary entry key or value already exists: "+key );
        return false;
    }
}

Dictionary.prototype.remove = function(key){
    delete this.keyValues[key];
    this.keyMap = null;
}

Dictionary.prototype.update = function(key, value){
    if(typeof this.getValue(key) != false && this.getKey(value) === false){
        this.keyValues[key] = value;
        return true;
    } return false;
}

Dictionary.prototype.get = function(identifier){
    if(typeof this.keyValues[identifier] != 'undefined'){
        return identifier;
    } else {
        identifier = this.getKey(identifier);
        if(identifier !== false){
            return identifier;
        }
    } return undefined;
}

Dictionary.prototype.getValue = function(key){
    if(typeof this.keyValues[key] != 'undefined'){
        return this.keyValues[key];
    } return undefined;
}

Dictionary.prototype.getKey = function(value){
    if(this.keyMap == null){
        this.keyMap = Object.keys(this.keyValues);
    }    

    if(this.keyMap.length <= 0){ 
        return undefined; 
    }

    var temp = this.keyValues;
    return this.keyMap.find(function(key){ 
        return temp[key] === value; 
    });
}


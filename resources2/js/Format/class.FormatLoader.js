function GDE_FormatLoader(options){
	
	this.elements = {};
	this.callback;
	this.format = null;
	this.formatElementsLoaded = 0;
	this.ordered = null;
	this.reversed = null;

	this.init = function(options){
		if(typeof options.callback !== 'undefined'){
			this.callback = options.callback;
		}
	};

	if (!(this instanceof GDE_FormatLoader)) { throw('Constructor called without "new"'); }
	return this.init.apply(this, [options]);
}

GDE_FormatLoader.prototype.load = function(context, name){
	let format = this.easySample[name];
	this.set(context, format);
}


GDE_FormatLoader.prototype.loaded = function(total){
	this.formatElementsLoaded++;
	if(this.formatElementsLoaded === total){
		if(typeof this.callback === 'function'){
			this.callback();
		}		
	}
};

GDE_FormatLoader.prototype.set = function(context, format){
	this.formatElementsLoaded = 0;
	this.format = format;

	let elements = {};
	for(let index in this.elements){
		if(this.elements.hasOwnProperty(index)){
			this.elements[index].setEvents({});
			if(this.elements[index].getName() == 'background'){
				elements[0] = this.elements[index];
				this.elements[index] = null;
				delete this.elements.background;
			} else if(this.elements[index].type == 'text'){
				this.elements[index].deregister();
			}
		}
	}

	let offsetLeft = context.offsetLeft();
	let offsetTop = context.offsetTop();
	let indexOfset = (elements.hasOwnProperty(0)) ? 1 : 0;
	for(let i = 0; i < format.elements.length; i++){
		if(typeof format.elements[i].x !== 'undefined'){
			format.elements[i].x += offsetLeft;
		} if(typeof format.elements[i].y !== 'undefined'){
			format.elements[i].y += offsetTop;
		}
			
		if(format.elements[i].type === 'image' || format.elements[i].type === 'overlay'){
			format.elements[i].imageLoaded = this.loaded.bind(this, format.elements.length);
		}

		if(format.elements[i].type === 'text'){
			elements[i + indexOfset] = new Canvas_TextElement(context.getCanvas(), format.elements[i]);
		} else if(format.elements[i].type === 'image' || format.elements[i].type === 'overlay') {
			elements[i + indexOfset] = new Canvas_ImageElement(context.getCanvas(), format.elements[i]);
		}

		if(format.elements[i].type === 'text'){
			setTimeout(this.loaded.bind(this, format.elements.length), 0);
		}
		
		if(typeof format.elements[i].x !== 'undefined'){
			format.elements[i].x -= offsetLeft;
		} if(typeof format.elements[i].y !== 'undefined'){
			format.elements[i].y -= offsetTop;
		}
	
		elements[i].setEvents(context);
	}

	this.elements = elements;
	this.ordered = null;
};

GDE_FormatLoader.prototype.get = function(){
	return this.format;
};

GDE_FormatLoader.prototype.draw = function(){
	let order = this.elementsInOrder(true);
	for(index = 0; index < order.length; index++){
		order[index].redraw();
	}
};

GDE_FormatLoader.prototype.elementsInOrder = function(reverse){
	if(typeof reverse === 'undefined'){ reverse = false; }
	if(this.ordered === null){
		let order = [];
		for(let index in this.elements){
			if(this.elements.hasOwnProperty(index)){
				order.push(this.elements[index]);
			}
		}
		order.sort(function compare(a,b) {
			if (a.order > b.order)
				return 1;
			if (a.order < b.order)
				return -1;
			return 0;
		});

		this.ordered = order;
		this.reversed = order.slice().reverse();
	}
	return (reverse === true) ? this.reversed : this.ordered;
};



GDE_FormatLoader.prototype.easySample = {
	'Basic Example' : {
		name : 'Basic Example',
		size : {
			width : 400,
			height : 400
		},
		category : 'Facebook',
		allowFlowControl : true,
		variaty : ['Basic Example 2'],
		elements : [ 
			{ 
				type : 'image',
				name : 'Goodday Logo',
				resizable : false,
				draggable : false,
				order : 3,
				x : 290,
				y : 290,
				width : 100,
				height : 100,
				src : 'resources/img/logo.png'
			}, 
			{ 
				type : 'image',
				name : 'Goodday Overlay',
				resizable : false,
				draggable : false,
				order : 2,
				x : 0,
				y : 0,
				width : 400,
				height : 400,
				src : 'resources/img/overlay.png'
			}
		]
	},
	'Basic Example 2' : {
		name : 'Basic Example 2',
		size : {
			width : 400,
			height : 400
		},
		allowFlowControl : true,
		variaty : ['Basic Example 3'],
		category : 'Facebook',
		elements : [ 
			// { 
			// 	type : 'image',
			// 	name : 'background',
			// 	resizable : true,
			// 	draggable : true,
			// 	order : 0,
			// 	src : 'resources/img/banner1.png'
			// }, 
			// { 
			// 	type : 'image',
			// 	name : 'Goodday Logo',
			// 	resizable : false,
			// 	draggable : false,
			// 	order : 3,
			// 	x : 10,
			// 	y : 10,
			// 	width : 100,
			// 	height : 100,
			// 	src : 'resources/img/logo.png'
			// }, 
			// { 
			// 	type : 'image',
			// 	name : 'Goodday Overlay',
			// 	resizable : false,
			// 	draggable : false,
			// 	order : 2,
			// 	x : 0,
			// 	y : 0,
			// 	width : 400,
			// 	height : 400,
			// 	src : 'resources/img/overlay2.png'
			// }, 
			{ 
				type : 'text',
				name : 'Tekst',
				font : 'Arial',
				fontSize : '30px',
				textAlign : 'left',
				color : '#000',
				placeholder : 'Type here..ik ben lesley%f%wist ik maar wat te doen%f%met mijn leven',
				resizable : false,
				draggable : false,
				clickable : true,
				order : 4,
				x : 100,
				y : 204,
				width : 200,
				height : 120,
			}
			// ,
			// { 
			// 	type : 'text',
			// 	name : 'Tekst 2',
			// 	font : 'Arial',
			// 	fontSize : '16px',
			// 	color : '#ffffff',
			// 	placeholder : 'Type here..',
			// 	resizable : false,
			// 	draggable : false,
			// 	clickable : true,
			// 	order : 5,
			// 	x : 80,
			// 	y : 240,
			// 	width : 250,
			// 	height : 100,
			// }
		]
	},
	'Basic Example 3' : {
		name : 'Basic Example 3',
		size : {
			width : 400,
			height : 400
		},
		variaty : ['Basic Example'],
		allowFlowControl : true,
		category : 'Facebook',
		elements : [ 
			{ 
				type : 'text',
				name : 'Tekst 1',
				font : 'Arial',
				fontSize : '32px',
				color : '#ffffff',
				placeholder : 'Type here..',
				resizable : false,
				draggable : false,
				clickable : true,
				order : 4,
				x : 80,
				y : 200,
				width : 320,
				height : 48,
			},
			{ 
				type : 'text',
				name : 'Tekst 2',
				font : 'Arial',
				fontSize : '16px',
				color : '#ffffff',
				placeholder : 'Type here..',
				resizable : false,
				draggable : false,
				clickable : true,
				order : 5,
				x : 80,
				y : 240,
				width : 250,
				height : 100,
			}
		]
	}
};
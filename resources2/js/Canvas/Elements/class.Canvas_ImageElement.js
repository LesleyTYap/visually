function Canvas_ImageElement(canvas, options){	
	this.imageData = null;
	this.keepAspectRatio = true;

	this.init = function(canvas, options){
		Canvas_Element.apply(this, [canvas, options]);
	    if(typeof options !== 'undefined'){
	    	this.setOptions(options);
		}
		
	    return this;
	};

	this.getImageData = function(){
		if(this.imageData === null){
			let canvas = document.createElement('canvas');
			canvas.width = this.canvas.canvasWidth();
			canvas.height = this.canvas.canvasHeight();
			
			let context = canvas.getContext('2d');
			let dimensions = this.getDimensions();
			context.drawImage(
				this.element,
		    	this.elementPosition.x, 
		    	this.elementPosition.y, 
		    	dimensions.width, 
		    	dimensions.height
		    );
			this.imageData = context.getImageData(
				0, 
				0, 
				this.canvas.canvasWidth(), 
				this.canvas.canvasHeight()
			);
			// document.body.appendChild(canvas);
		} else {
			return this.imageData;
		}
	}

	this.currentPixelEmpty = function(x, y){
		if(this.element === null){ return true; }
		if(this.getType() !== 'image' && this.getType() !== 'overlay'){ return false; }
		let data = this.getImageData();
		if(typeof data === 'undefined'){ return true; }
		if(x < 0 || y < 0){ return true; }
		if(typeof data === 'undefined'){ return true; }

		
		let coords = this.canvas.getColorOnCoordinate(x, y, this.canvas.canvasWidth());
		if(!coords){ return true; }
		
		return data.data[coords.a / this.scale.width] === 0 ? true : false;
	};

	this.setOptions = function(format){
		if(typeof format.src !== 'undefined'){
			this.setImage(format.src, format.imageLoaded);
		}
	};

	this.setImage = function(src, callback){
		let image = new Image();
		image.onload = function(){
			this.element = image;
			this.position();
			if(typeof callback === 'function'){
				callback();
			}
	    }.bind(this);
		image.src = src
	};

	this.isDraggable = function(x, y){
		if(this.currentPixelEmpty(x, y)){ return false; }
	    return this._isDraggable(x, y);
    };
    
	this.isTargetable = function(x, y){
		var adjustment = 15;
		if(this._isTargetable(x, y) || !this.currentPixelEmpty(x, y)){
			return true;
		} else if(
			!this.currentPixelEmpty(x - adjustment, y - adjustment) ||
			!this.currentPixelEmpty(x + adjustment, y - adjustment) ||
			!this.currentPixelEmpty(x - adjustment, y + adjustment) ||
			!this.currentPixelEmpty(x + adjustment, y + adjustment)){
			return true;	
		} 
		return  false;
	};

	this.drawElement = function(dimensions){
		if(this.state === 1 || this.state === 2){
			this.drawOutlines(dimensions);
		}

		this.canvas.context.drawImage(
			this.element,
			this.elementPosition.x, 
			this.elementPosition.y, 
			dimensions.width, 
			dimensions.height
		);
	};

	this.clearActive = function(){
		this._clearActive();
	};

	this.setEvents = function(context){
		scrollEvent =  new Canvas_ScrollEvent(this, context)
		this.events = {
			mousewheel : scrollEvent,
			DOMMouseScroll : scrollEvent,
			mousedown : new Canvas_DragEvent(this, context),
		};
	};

	this.drawOutlines = function(dimensions){
	    let size = 2;
	    let lineWidth = 2;
		let xs = this.elementPosition.x - lineWidth;
	    let ys = this.elementPosition.y - lineWidth;

		let width = dimensions.width + (lineWidth * 2);
		let height = dimensions.height + (lineWidth * 2);

		let hWidth = (dimensions.width / 2) + (lineWidth * 2);
		let hHeight = (dimensions.height / 2) + (lineWidth * 2);

		this.canvas.context.lineWidth =  lineWidth;
		this.canvas.context.fillStyle = '#2381e0';
		this.canvas.context.strokeStyle = '#2381e0';

		if(!this.keepAspectRatio){
			this.canvas.context.beginPath();
				this.canvas.context.arc(xs + hWidth, ys, size * 1.5, 0, 2 * Math.PI);
				this.canvas.context.fill();
			this.canvas.context.closePath();
		}
		this.canvas.context.beginPath();
			this.canvas.context.arc(xs, ys, size, 0, 2 * Math.PI);
			this.canvas.context.fill();
			
			this.canvas.context.moveTo(xs, ys);
			this.canvas.context.lineTo(xs + width, ys);
			this.canvas.context.stroke();
		this.canvas.context.closePath();


		if(!this.keepAspectRatio){
			this.canvas.context.beginPath();
				this.canvas.context.arc(xs + width, ys + hHeight, size * 1.5, 0, 2 * Math.PI);
				this.canvas.context.fill();
			this.canvas.context.closePath();
		}
		this.canvas.context.beginPath();
			this.canvas.context.arc(xs + width, ys, size, 0, 2 * Math.PI);
			this.canvas.context.fill();

			this.canvas.context.moveTo(xs + width, ys);
			this.canvas.context.lineTo(xs + width, ys + height);
			this.canvas.context.stroke();
		this.canvas.context.closePath();


		if(!this.keepAspectRatio){
			this.canvas.context.beginPath();
				this.canvas.context.arc(xs + hWidth, ys + height, size * 1.5, 0, 2 * Math.PI);
				this.canvas.context.fill();
			this.canvas.context.closePath();
		}
		this.canvas.context.beginPath();
			this.canvas.context.arc(xs + width, ys + height, size, 0, 2 * Math.PI);
			this.canvas.context.fill();

			this.canvas.context.moveTo(xs + width, ys + height);
			this.canvas.context.lineTo(xs, ys + height);
			this.canvas.context.stroke();
		this.canvas.context.closePath();


		if(!this.keepAspectRatio){
			this.canvas.context.beginPath();
				this.canvas.context.arc(xs, ys + hHeight, size * 1.5, 0, 2 * Math.PI);
				this.canvas.context.fill();
			this.canvas.context.closePath();
		}
		this.canvas.context.beginPath();
			this.canvas.context.arc(xs, ys + height, size, 0, 2 * Math.PI);
			this.canvas.context.fill();

			this.canvas.context.moveTo(xs, ys + height);
			this.canvas.context.lineTo(xs, ys);
			this.canvas.context.stroke();
		this.canvas.context.closePath();

		
	};

	if (!(this instanceof Canvas_ImageElement)) { throw('Constructor called without "new"'); }
	return this.init.apply(this, [canvas, options]);
}
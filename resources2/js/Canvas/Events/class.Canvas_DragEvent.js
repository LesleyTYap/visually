function Canvas_DragEvent(element, workspace){
    this.element;

	this.currentEnd;
	this.currentOn;

	this.init = function(element, workspace){
        this.element = element;
        this.workspace = workspace;


		return this;
	};
	if (!(this instanceof Canvas_DragEvent)) { throw('Constructor called without "new"'); }
	return this.init.apply(this, [element, workspace]);
}

Canvas_DragEvent.prototype.getElement = function(){
    return this.element;
}

Canvas_DragEvent.prototype.getWorkspace = function(){
    return this.workspace;
}

Canvas_DragEvent.prototype.run = function(e){
    this.start(e);
}

Canvas_DragEvent.prototype.start = function(e){
    // e.stopPropagation();
    canMouseX = parseInt(e.clientX - this.getElement().canvasOffset.left);
    canMouseY = parseInt(e.clientY - this.getElement().canvasOffset.top);

    if(this.getElement().state === 1){
        this.getElement().state = 2;
    } 

    if(this.getElement().state === 1 || this.getElement().state === 2){ 
        if(this.getElement().isResizable() && this.getElement().canResize(canMouseX, canMouseY)){
            this.getElement().isResizing = true;
        }
        if(this.getElement().isDraggable(canMouseX, canMouseY)){
            this.getElement().isDragging = true;
        }

        
    }

    if(this.getElement().state === -1){ this.getElement().state = 0; }
    if(this.getElement() instanceof Canvas_TextElement && this.getElement().inTextarea(canMouseX, canMouseY)){
        this.getElement().getEditor().enable();
    }

    this.getElement().mousePosition.x = canMouseX;
    this.getElement().mousePosition.y = canMouseY;
  
    this.currentOn = function(e){ this.on.call(this, e); }.bind(this);
    this.currentEnd = function(e){ this.end.call(this, e); }.bind(this);

    this.getWorkspace().getCanvas().element.addEventListener('mousemove', this.currentOn , false);
    this.getWorkspace().getCanvas().element.addEventListener('mouseup', this.currentEnd, false);
    this.getWorkspace().getCanvas().element.addEventListener('blur', this.currentEnd, false);
}


Canvas_DragEvent.prototype.on = function(e){
    // e.stopPropagation();
    if(this.getElement().state === 0){ return; }
    if(this.getElement().state === 2){
        this.getElement().state = 1;
      }
  
      canMouseX = parseInt(e.clientX - this.getElement().canvasOffset.left);
      canMouseY = parseInt(e.clientY - this.getElement().canvasOffset.top);	  	

      // if the drag flag is set, clear the canvas and draw the image
      if(this.getElement().element !== null && this.getElement().isResizing){
          this.getElement().resize(this.getWorkspace(), canMouseX, canMouseY);
      } else if(this.getElement().element !== null && this.getElement().isDragging){
          this.getElement().move(this.getWorkspace(), canMouseX, canMouseY);
    }

    this.getElement().mousePosition.x = canMouseX;
    this.getElement().mousePosition.y = canMouseY;
}

Canvas_DragEvent.prototype.end = function(e){
    // e.stopPropagation();
    
    canMouseX = parseInt(e.clientX - this.getElement().canvasOffset.left);
    canMouseY = parseInt(e.clientY - this.getElement().canvasOffset.top);
    if(this.getElement() instanceof Canvas_TextElement){
        if(!this.getElement().inTextarea(canMouseX, canMouseY)){
            this.getElement().getEditor().disable();
        } else {
            this.getElement().state = 1;
            this.getWorkspace().draw();
        }
    } else {
        if(this.getElement().state === 0 && (this.getElement().isDraggable(canMouseX, canMouseY) || this.getElement().clickable)){
            this.getElement().state = 1;
            this.getWorkspace().draw();
        } else if(this.getElement().state === 2) {
        
            this.getElement().state = 0;
            this.getWorkspace().draw();
        }
    }
    
    
  

    if((this.getElement().getType() === 'image' || this.getElement().getType() === 'overlay') && (this.getElement().isResizing || this.getElement().isDragging)){
        this.getElement().imageData = null;
    }
    this.getWorkspace().getCanvas().element.style.cursor = "pointer";
    this.getElement().isResizing = false;
    this.getElement().isDragging = false;
    this.getWorkspace().getCanvas().element.removeEventListener('mousemove', this.currentOn, false);
    this.getWorkspace().getCanvas().element.removeEventListener('mouseup', this.currentEnd, false);
    this.getWorkspace().getCanvas().element.removeEventListener('blur', this.currentEnd, false);
}

Canvas_DragEvent.prototype.deregister = function(){
    this.getWorkspace().getCanvas().element.removeEventListener('mousemove', this.currentOn, false);
    this.getWorkspace().getCanvas().element.removeEventListener('mouseup', this.currentEnd, false);
    this.getWorkspace().getCanvas().element.removeEventListener('blur', this.currentEnd, false);
}
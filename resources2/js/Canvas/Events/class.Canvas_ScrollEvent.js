function Canvas_ScrollEvent(element, workspace){
    this.element;

	this.currentEnd;
	this.currentOn;

	this.init = function(element, workspace){
        this.element = element;
        this.workspace = workspace;


		return this;
	};
	if (!(this instanceof Canvas_ScrollEvent)) { throw('Constructor called without "new"'); }
	return this.init.apply(this, [element, workspace]);
}

Canvas_ScrollEvent.prototype.getElement = function(){
    return this.element;
}

Canvas_ScrollEvent.prototype.getWorkspace = function(){
    return this.workspace;
}

Canvas_ScrollEvent.prototype.run = function(e){
    this.scroll(e);
}

Canvas_ScrollEvent.prototype.scroll = function(e){
	if(this.getElement().state === 0){ return; }
	if(!this.getElement().isResizable()){ return; }
    var delta = e.wheelDelta ? e.wheelDelta / 40 : e.detail ? -e.detail : 0;
    if (delta) this.getElement().zoom.call(this.getElement(), this.getWorkspace(), delta, { left : this.getElement().elementPosition.x, top : this.getElement().elementPosition.y });
    return e.preventDefault() && false;
}
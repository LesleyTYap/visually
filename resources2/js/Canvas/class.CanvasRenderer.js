function GDE_CanvasRenderer(){
    this.drawable;
    
    this.init = function(){ 
        this.disable();
        return this; 
    };
    
    if (!(this instanceof GDE_CanvasRenderer)) { throw('Constructor called without "new"'); }
    return this.init.apply(this);
}

GDE_CanvasRenderer.prototype.isDrawable = function(){
    return this.drawable;
}

GDE_CanvasRenderer.prototype.disable = function(){
    this.drawable = false;
}

GDE_CanvasRenderer.prototype.enable = function(){
    this.drawable = true;
}

GDE_CanvasRenderer.prototype.preRender = function(context){
    if(this.isDrawable()){
        console.log(context);
        context.draw();
    }
}

GDE_CanvasRenderer.prototype.clear = function(context){
	context.getCanvas().context.clearRect(0, 0, context.getCanvas().element.width, context.getCanvas().element.height);
};
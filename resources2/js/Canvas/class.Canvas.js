function GDE_Canvas(element, context){
	this.element;
	this.context;
	this.padding;
	this.preRender = null;
	this.background = null;

	this.init = function(element, context){
		this.element = element
		this.context = this.element.getContext(context);

		let size = { width : this.canvasWidth(), height : this.canvasHeight() };
		this.element.width = size.width;
		this.element.height = size.height;
		return this;
	};

	if (!(this instanceof GDE_Canvas)) { throw('Constructor called without "new"'); }
	return this.init.apply(this, [element, context]);
}

GDE_Canvas.prototype.getStandard = function(image){
	let standard = this.findFit({ width : image.width, height : image.height });

	if(this.canvasWidth() > standard.width || this.canvasHeight() > standard.height){
		standard.outerWidth = this.canvasWidth();
		standard.outerHeight = this.canvasHeight();
	}
	return standard;
};

GDE_Canvas.prototype.findFit = function(base){
	let max = { width : this.canvasInnerWidth(), height : this.canvasInnerHeight() };	
	
	if(base.width > max.width){
		base.height = (max.width / base.width) * base.height;
		base.width = max.width;
		return this.findFit(base);
	} else if(base.height > max.height){
		base.width = (max.height / base.height) * base.width;
		base.height = max.height;
		return this.findFit(base);
	}
	return base;
};

GDE_Canvas.prototype.getColorOnCoordinate = function(x, y, width){
	if(x < 0 || x > this.canvasWidth() || y < 0 || y > this.canvasHeight()){
		return false;
	}
	let position = ((y * (width * 4)) + (x * 4));
	return {r:position, g:position + 1, b:position + 2, a:position + 3};
};

GDE_Canvas.prototype.canvasInnerWidth = function(){
	let retract = {left : this.getPadding('left'), right : this.getPadding('right')};
	return this.element.clientWidth - retract.left - retract.right;
};

GDE_Canvas.prototype.canvasInnerHeight = function(){
	let retract = {top : this.getPadding('top'), bottom : this.getPadding('bottom')};
	return this.element.clientHeight - retract.top - retract.bottom;
};

GDE_Canvas.prototype.canvasWidth = function(){
	return this.element.clientWidth;
};

GDE_Canvas.prototype.canvasHeight = function(){
	return this.element.clientHeight;
};

GDE_Canvas.prototype.setPadding = function(padding, type){
	let isValidType = (['left', 'right', 'top', 'bottom'].indexOf(type) == -1);
	if(typeof padding === 'number' && (typeof type === 'undefined' || isValidType)){
		this.padding = padding;
		return;
	}

	if(typeof this.padding === 'number' || typeof this.padding === 'undefined' || this.padding === null) {
		this.padding = {};
	}
	
	if(typeof type === 'undefined' && typeof padding !== 'number'){
		this.padding.left = typeof padding.left !== 'undefined' ? padding.left : this.padding.left;
		this.padding.right = typeof padding.right !== 'undefined' ? padding.right : this.padding.right;
		this.padding.top = typeof padding.top !== 'undefined' ? padding.top : this.padding.top;
		this.padding.bottom = typeof padding.bottom !== 'undefined' ? padding.bottom : this.padding.bottom;
		
	} else if(typeof type !== 'undefined' && isValidType) {
		this.padding[type] = padding;
	}
};

GDE_Canvas.prototype.getPadding = function(type){
	if(typeof this.padding === 'undefined' || this.padding === null){ return 0; }

	if(typeof this.padding !== 'number' && typeof type !== 'undefined'){

		if(typeof this.padding[type] === 'undefined' || this.padding[type] === null){
			return 0;
		} else { this.padding[type] };

	} else if(typeof this.padding === 'number') {
		return this.padding;
	}
	return this.padding;
};

GDE_Canvas.prototype.findCenter = function(image){
	return {
		left : (this.element.width - image.width ) / 2,
		top : (this.element.height - image.height) / 2,
	}
}
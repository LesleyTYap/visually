function assignCommands(element, editor){
    commands = new CommandList();

	var escape = new KeyCommand({ 
		name : "Escape", 
		keyCode : 27, 
		command : editor.actions.escape
	});
	commands.addCommand(escape);

	var left = new KeyCommand({ 
		name : "ArrowLeft", 
		keyCode : 37, 
		command : editor.actions.left
	});
	commands.addCommand(left);

	var up = new KeyCommand({ 
		name : "ArrowUp", 
		keyCode : 38, 
		command : editor.actions.up
	});
	commands.addCommand(up);

	var right = new KeyCommand({ 
		name : "ArrowRight", 
		keyCode : 39, 
		command : editor.actions.right
	});
	commands.addCommand(right);

	var down = new KeyCommand({ 
		name : "ArrowDown", 
		keyCode : 40, 
		command : editor.actions.down
	});
	commands.addCommand(down);

	
	var leftMouseDown = new KeyCommand({ 
		name : "LeftMouseDown", 
		keyCode : -1, 
		command : editor.actions.mouse.down
	});		
	commands.addCommand(leftMouseDown);

	var leftMouseUp = new KeyCommand({ 
		name : "LeftMouseUp", 
		keyCode : -2, 
		command : editor.actions.mouse.up
	});	
	commands.addCommand(leftMouseUp);

	var mouseMouse = new KeyCommand({ 
		name : "MouseMove", 
		keyCode : -3, 
		command :  editor.actions.mouse.move 
	});	
	commands.addCommand(mouseMouse);

	var enter = new KeyCommand({ 
		name : "Enter", 
		keyCode : 13, 
		command : editor.actions.enter
	});
	commands.addCommand(enter);

	var copy = new KeyCommand({ 
		name : "c-c", 
		keyCode : 'c-67', 
		command : editor.actions.copy
	});
	commands.addCommand(copy);

	var paste = new KeyCommand({ 
		name : "c-v", 
		keyCode : 'c-86', 
		command : editor.actions.paste 
	});
	commands.addCommand(paste);	

	var cut = new KeyCommand({ 
		name : "c-x", 
		keyCode : 'c-88', 
		command : editor.actions.cut
	});
	commands.addCommand(cut);	

	var selectAll = new KeyCommand({ 
		name : "c-a", 
		keyCode : 'c-65', 
		command : editor.actions.select.all 
	});
	commands.addCommand(selectAll);	

	var backspace = new KeyCommand({ 
		name : "Backspace", 
		keyCode : 8, 
		command : editor.actions.backspace
	});
	commands.addCommand(backspace);
	
	
	var delete_ = new KeyCommand({ 
		name : "Delete", 
		keyCode : 46, 
		command : editor.actions.delete 
	});
	commands.addCommand(delete_);
	
	var end = new KeyCommand({ 
		name : "End", 
		keyCode : 35, 
		command : editor.actions.end 
	});
	commands.addCommand(end);
	
	var home = new KeyCommand({ 
		name : "Home", 
		keyCode : 36, 
		command : editor.actions.home 
	});
	commands.addCommand(home);


	commands.setContext(editor);
		
	deligator = new EventDelegator(commands);
	deligator.addDelegator(element);
	return deligator;
}

function removeCommands(element, deligator){
	deligator.removeDelegator(element);
}
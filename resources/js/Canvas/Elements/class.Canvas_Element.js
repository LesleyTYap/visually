function Canvas_Element(canvas, options){
	this.element = null;
	this.canvas;
	this.canvasOffset;
	this.elementPosition = {x : 0, y : 0};
	this.events;

	this.name;
	this.clickable = false;
	this.draggable = false;
	this.resizable = false;
	this.type;
	this.order = 0;
	this.height = null;
	this.width = null;

	this.zoomFactor = 1.1;
	
	this.resizeDirection;
	this.scale = { width : 1, height : 1 };
	
	this.state = -1; //0 inactive - 1 active - 2 transition

	this._init = function(canvas, options){
		this.canvas = canvas;		
		this.canvasOffset = this.canvas.element.getBoundingClientRect();		
	    this.isDragging = false;
	    
	    this.isResizing = false;
		this.resizeDirection = {left : false, top : false, right : false, bottom : false};	

		this.events = {};

	    if(typeof options !== 'undefined'){
	    	this._setOptions(options);
	    }

	    return this;
	};

	this.hasEvent = function(name){
		return (typeof this.events[name] != 'undefined') ? true : false;
	};

	this.doEvent = function(event, type){
		if(typeof this.events[type].run != 'undefined'){
			this.events[type].run(event);
		}
	}

	this._setOptions = function(format){
		if(typeof format.type !== 'undefined'){
			this.setType(format.type);
		} if(typeof format.order !== 'undefined'){
			this.setOrder(format.order);
		} if(typeof format.resizable !== 'undefined'){
			this.setResizable(format.resizable);
		} if(typeof format.draggable !== 'undefined'){
			this.setDraggable(format.draggable);
		} if(typeof format.name !== 'undefined'){
			this.setName(format.name);
		} if(typeof format.width !== 'undefined'){
			this.setWidth(format.width);
		} if(typeof format.height !== 'undefined'){
			this.setHeight(format.height);
		} if(typeof format.x !== 'undefined' && typeof format.y !== 'undefined'){
			this.elementPosition.x = format.x;
			this.elementPosition.y = format.y;
		} else if(this.element !== null){
			this.center();
		}

		if(this.draggable || this.resizable) {
			this.clickable = true;
		} else if(typeof format.clickable !== 'undefined'){
			this.clickable = format.clickable;
		}
	};

	this._clearActive = function(){
	
	}

	this._drawElement = function(dimensions){
		if(this.state === 1 || this.state === 2){
			this.drawOutlines(dimensions);
		}

		this.drawElement();
	};

	this.getType = function(){
		return this.type;
	};

	this.setType = function(type){
		this.type = type;
	};

	this.getOrder = function(){
		return this.order;
	};

	this.setOrder = function(order){
		this.order = order;
	};

	this.getName = function(){
		return this.name;
	};

	this.setName = function(name){
		this.name = name;
	};

	this.setResizable = function(value){
		this.resizable = (value === true) ? true : false;
	};

	this.isResizable = function(){
		return this.resizable;
	};

	this.isSelectable = function(){
		return (this.isResizable() || this.draggable) ? true : false;
	};

	this._getWidth = function(){
		return this.width;
	};

	this.setWidth = function(width){
		this.width = width;
	};

	this.getHeight = function(){
		return this.height;
	};

	this.setHeight = function(height){
		this.height = height;
	};

	this._isTargetable = function(x, y){
		return this.isDraggable(x, y);
	};

	this._isDraggable = function(x, y){
		if(this.draggable !== true) { return false; }
		if(this.element === null){ return false; }
		let dimensions = this.getDimensions();

		let position = {left : 0, top : 0};
		if(this.elementPosition.x === 0 && this.elementPosition.y === 0){
			position = this.canvas.findCenter(dimensions);
		}

	    if(x >= position.left + this.elementPosition.x && x <= position.left + this.elementPosition.x + dimensions.width){
	    	if(y >= position.top + this.elementPosition.y && y <= position.top + this.elementPosition.y + dimensions.height){
	    		return true;
	    	}
	    }
	    return false;
	};

	//if (!(this instanceof Canvas_Element)) { throw('Constructor called without "new"'); }
	return this._init.apply(this, [canvas, options]);
}



Canvas_Element.prototype.getWidth = function(){
	return this._getWidth();
};

Canvas_Element.prototype.setEvents = function(events){
	this.events = events;
}

Canvas_Element.prototype.getEvents = function(){
	return this.events;
}

Canvas_Element.prototype.clearActive = function(){
	this._clearActive();
}

Canvas_Element.prototype.cursorBehaviour = function(position, offset){
	canMouseX = parseInt(position.x - this.canvasOffset.left);
	canMouseY = parseInt(position.y - this.canvasOffset.top);

	let draggable = this.isDraggable(canMouseX, canMouseY);
	if(this.state === 1 || this.state === 2){
	  	if(this.isResizable() && this.canResize(canMouseX, canMouseY)){
	  		return this.getResizeCursor();
	  	} else if(draggable) {
	  		return "move";
	  	} else if(!draggable) {
	  		return "pointer";
	  	}
	} else {
		return "pointer";
	}
};

Canvas_Element.prototype.getDimensions = function(){
	let dimensions;
	if(this.getHeight() !== null && this.getWidth() !== null){
		dimensions = {
			width : this.width * this.scale.width,
			height : this.height * this.scale.height
		};
	} else {
		dimensions = this.canvas.getStandard(this.element);
		dimensions.width *= this.scale.width;
		dimensions.height *= this.scale.height;
	}
	return dimensions;
};

Canvas_Element.prototype.position = function(){
	if(this.elementPosition.x === 0 && this.elementPosition.y === 0){
		this.center();
	}

	this.redraw();
};

Canvas_Element.prototype.center = function(){
	let dimensions = this.getDimensions();
	this.elementPosition.x = (this.canvas.canvasWidth() - dimensions.width) / 2;
	this.elementPosition.y = (this.canvas.canvasHeight() - dimensions.height) / 2;
};



Canvas_Element.prototype.canResize = function(x, y){
	if(this.isResizing){ return true; }
	if(this.element === null){ return false; }
	let dimensions = this.getDimensions();

	let position = {left : 0, top : 0};
	if(this.elementPosition.x === 0 && this.elementPosition.y === 0){
		position = this.canvas.findCenter(dimensions);
	}

	let margin = 15;
	if(x >= position.left + this.elementPosition.x - margin && x <= position.left + this.elementPosition.x + margin){
		this.resizeDirection.left = true;
	} else { this.resizeDirection.left = false; }

	if(y >= position.top + this.elementPosition.y - margin && y <= position.top + this.elementPosition.y + margin){
		this.resizeDirection.top = true;
	} else { this.resizeDirection.top = false; }

	if(x <= position.left + this.elementPosition.x + dimensions.width + margin && x >= position.left + this.elementPosition.x + dimensions.width - margin){
		this.resizeDirection.right = true;
	} else { this.resizeDirection.right = false; }
	
	if(y <= position.top + this.elementPosition.y + dimensions.height + margin && y >= position.top + this.elementPosition.y + dimensions.height - margin){
		this.resizeDirection.bottom = true;
	} else { this.resizeDirection.bottom = false; }
	

	if(this.keepAspectRatio){
		if(this.resizeDirection.top && this.resizeDirection.right || 
			this.resizeDirection.top && this.resizeDirection.left || 
			this.resizeDirection.bottom && this.resizeDirection.right || 
			this.resizeDirection.bottom && this.resizeDirection.left){
			return true;
		}
		return false;
	}
	return (this.resizeDirection.left || this.resizeDirection.top || this.resizeDirection.right || this.resizeDirection.bottom) ? true : false;
};

Canvas_Element.prototype.isDragging;
Canvas_Element.prototype.getDraggable = function(){
	return this.draggable;
};
Canvas_Element.prototype.setDraggable = function(value){
	this.draggable = value;
};

Canvas_Element.prototype.mousePosition = {x : 0, y : 0};


Canvas_Element.prototype.isResizing = false;
Canvas_Element.prototype.getResizeCursor = function(){
	if(this.resizeDirection.left && this.resizeDirection.top){
		return 'nw-resize';
	} else if(this.resizeDirection.right && this.resizeDirection.top){
		return 'ne-resize';
	} else if(this.resizeDirection.left && this.resizeDirection.bottom){
		return 'sw-resize';
	} else if(this.resizeDirection.right && this.resizeDirection.bottom){
		return 'se-resize';
	} else if(this.resizeDirection.left){
		return 'w-resize';
	} else if(this.resizeDirection.top){
		return 'n-resize';
	} else if(this.resizeDirection.right){
		return 'e-resize';
	} else if(this.resizeDirection.bottom){
		return 's-resize';
	}
};

Canvas_Element.prototype.isTargetable = function(x, y){
	return this._isTargetable(x, y);
};

Canvas_Element.prototype.drawElement = function(dimensions){ };

Canvas_Element.prototype.redraw = function(){
	if((this.element && this.type == 'image') || this.type == 'text'){
		let dimensions = this.getDimensions();
		this.drawElement(dimensions);
	}
};

Canvas_Element.prototype.drawOutlines = function(dimensions){
    let size = 2;
    let lineWidth = 2;
	let xs = this.elementPosition.x - lineWidth;
    let ys = this.elementPosition.y - lineWidth;

	let width = dimensions.width + (lineWidth * 2);
	let height = dimensions.height + (lineWidth * 2);

	let hWidth = (dimensions.width / 2) + (lineWidth * 2);
	let hHeight = (dimensions.height / 2) + (lineWidth * 2);

	this.canvas.context.lineWidth =  lineWidth;
	this.canvas.context.fillStyle = '#2381e0';
	this.canvas.context.strokeStyle = '#2381e0';

	this.canvas.context.beginPath();
		this.canvas.context.arc(xs, ys, size, 0, 2 * Math.PI);
		this.canvas.context.fill();
		
		this.canvas.context.moveTo(xs, ys);
		this.canvas.context.lineTo(xs + width, ys);
		this.canvas.context.stroke();
	this.canvas.context.closePath();

	this.canvas.context.beginPath();
		this.canvas.context.arc(xs + width, ys, size, 0, 2 * Math.PI);
		this.canvas.context.fill();

		this.canvas.context.moveTo(xs + width, ys);
		this.canvas.context.lineTo(xs + width, ys + height);
		this.canvas.context.stroke();
	this.canvas.context.closePath();

	this.canvas.context.beginPath();
		this.canvas.context.arc(xs + width, ys + height, size, 0, 2 * Math.PI);
		this.canvas.context.fill();

		this.canvas.context.moveTo(xs + width, ys + height);
		this.canvas.context.lineTo(xs, ys + height);
		this.canvas.context.stroke();
	this.canvas.context.closePath();

	this.canvas.context.beginPath();
		this.canvas.context.arc(xs, ys + height, size, 0, 2 * Math.PI);
		this.canvas.context.fill();

		this.canvas.context.moveTo(xs, ys + height);
		this.canvas.context.lineTo(xs, ys);
		this.canvas.context.stroke();
	this.canvas.context.closePath();
};	

Canvas_Element.prototype.resize = function(context, x, y){
	let dimensions = this.canvas.getStandard(this.element);
  	let scaledDimensions = { 
		width : (dimensions.width * this.scale.width), 
		height : (dimensions.height * this.scale.height) 
	};

	if(this.elementPosition.x === 0 && this.elementPosition.y === 0){
		position = this.canvas.findCenter(dimensions);
		this.elementPosition.x = position.left;
		this.elementPosition.y = position.top;
	}

	let mod = { x : (x - this.mousePosition.x), y : (y - this.mousePosition.y) };
	let currentMod = { x : mod.x, y : mod.y };
	let ratio = dimensions.width / dimensions.height;
	if(this.keepAspectRatio){
		if(this.resizeDirection.top && this.resizeDirection.right || 
			this.resizeDirection.top && this.resizeDirection.left || 
			this.resizeDirection.bottom && this.resizeDirection.right || 
			this.resizeDirection.bottom && this.resizeDirection.left){
			mod.x = mod.y * ratio;
		}
	}

	let newDimensions = { width : scaledDimensions.width,  height : scaledDimensions.height };
	if(this.keepAspectRatio && this.resizeDirection.top && this.resizeDirection.right){
		newDimensions.width = scaledDimensions.width - mod.x;
		newDimensions.height = scaledDimensions.height - mod.y;
	} else if(this.keepAspectRatio && this.resizeDirection.bottom && this.resizeDirection.left){		
		newDimensions.width = scaledDimensions.width + mod.x;
		newDimensions.height = scaledDimensions.height + mod.y;
	} else { 
		if(this.resizeDirection.top){
			newDimensions.height = scaledDimensions.height - mod.y;
		} if(this.resizeDirection.left){
			newDimensions.width = scaledDimensions.width - mod.x;
		} if(this.resizeDirection.right){
			newDimensions.width = scaledDimensions.width + mod.x;
		} if(this.resizeDirection.bottom){
			newDimensions.height = scaledDimensions.height + mod.y;
		}
	}
	
	if((newDimensions.height / dimensions.height) < 0.01){
		return;
	} if((newDimensions.width / dimensions.width) < 0.01){
		return;
	} 

	if(this.resizeDirection.top || this.resizeDirection.bottom){
		if(this.resizeDirection.top){ 
			this.elementPosition.y += mod.y; 
		}

		this.scale.height = newDimensions.height / dimensions.height;
	} if(this.resizeDirection.left || this.resizeDirection.right){
	 	if(this.resizeDirection.left){ 
	 		if(this.keepAspectRatio && this.resizeDirection.bottom){
	 			this.elementPosition.x -= mod.x;
	 		} else {
	 			this.elementPosition.x += mod.x;
	 		} 
	 	}
		
		this.scale.width = newDimensions.width / dimensions.width;
	}
   	context.draw();
}
Canvas_Element.prototype.zoom = function(context, clicks, start){
	
	if(typeof start === 'undefined'){
		start = this.canvas.findCenter();
	}

    let factor = Math.pow(this.zoomFactor ,clicks);
    let dimensions = this.getDimensions();

	let resized = {
		width : dimensions.width * factor,
		height: dimensions.height * factor
	};

	if(factor > 1){
	   	this.elementPosition.x += ((dimensions.width - resized.width) / 2);
	    this.elementPosition.y += ((dimensions.height - resized.height) / 2);
	} else {
		this.elementPosition.x += ((dimensions.width - resized.width) / 2);
	    this.elementPosition.y += ((dimensions.height - resized.height) / 2);
	}

    this.scale.width *= factor;
    this.scale.height *= factor;
    
   	context.draw();
};

Canvas_Element.prototype.move = function(context, x, y){
	if(this.mousePosition.x !== null && this.mousePosition.y !== null){
		this.elementPosition.x += x - this.mousePosition.x;
		this.elementPosition.y += y - this.mousePosition.y;
	} else { 
		this.elementPosition.x = 0;
		this.elementPosition.Y = 0;
	}

   	context.draw();
}
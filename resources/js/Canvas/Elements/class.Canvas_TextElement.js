function Canvas_TextElement(canvas, options){

	this.editor;
	this.deligator;

	this.init = function(canvas, options){
		Canvas_Element.apply(this, [canvas, options]); 

		options.outputAdaptor = new Canvas_OutputAdaptor(this);
		this.editor = new GDE_Editor(options);		

		this.deligator = assignCommands(this.canvas.element, this.editor);

	    return this;
	};

	this.deregister = function(){
		removeCommands(this.canvas.element, this.deligator);
	}

	this.isStaticText = function(){
		return this.staticText;
	};

	this.setStaticText = function(isStatic){
		this.staticText = isStatic;
	};

	this.inTextarea = function(x, y){
		if(this.getType() !== 'text'){ return false; }
		let ys = this.elementPosition.y;
		if(this.isWritable()){
			ys -= parseInt(this.getEditor().fontSize);
		}
		if(x >= this.elementPosition.x && x <= this.elementPosition.x + this.getWidth()){
			if(y >= ys && y <= ys + this.getEditor().getContentHeight()){
				return true;
			}
		}
		return false;
	};

	this.getWidth = function(){
		let width = this._getWidth();
		return width; //- this.getEditor().padding.left - this.getEditor().padding.right; 
	}
	

	this.clearActive = function(){
		this._clearActive();
		this.editor.disable();
	};

	this.getEditor = function(){
		return this.editor;
	}

	this.isDraggable = function(x, y){
		let draggable, canDrag = this.draggable;
		this.draggable = true;
		draggable = this._isDraggable(x, y);
		this.draggable = canDrag;
	    return draggable;
    };

	this.setEvents = function(context){
		this.events = {
			mousedown : new Canvas_DragEvent(this, context),
		};
	};
    
	this.isTargetable = function(x, y){
		return (this._isTargetable(x, y) || this.inTextarea(x, y));
	};

	this.isSelectable = function(){
		return (this.isWritable() || this.isResizable() || this.draggable) ? true : false;
	};

	this.isWriting;
	this.isWritable = function(){
		return this.getType() === 'text' && !this.isStaticText() ? true : false;
	};

	this.drawElement = function(dimensions){		
		this.drawOutlines(dimensions);
		if(this.state === 1 || this.state === 2){
			this.drawOutlines(dimensions);
		}
		
		let fill = this.canvas.context.fillStyle;
        let font = this.canvas.context.font;
		
		this.canvas.context.fillStyle = this.getEditor().color;
		this.canvas.context.font = this.getEditor().fontSize + ' ' + this.getEditor().font;
		this.canvas.context.textAlign = this.getEditor().textAlign;

		var xPos;
		if(this.getEditor().textAlign == 'center') {
			xPos = this.elementPosition.x + (this.getWidth() * 0.5);
		} else if(this.getEditor().textAlign == 'right') {
			xPos = this.elementPosition.x + (this.getWidth());
			xPos -= this.getEditor().padding.right;
		} else {
			xPos = this.elementPosition.x;
			xPos += this.getEditor().padding.left;
		}

		let lines = this.getEditor().totalLines;
		if(this.getEditor().rendered[this.getEditor().rendered.length - 1] == ''){
			lines--;
		}
		
		for(let i = 0; i < lines; i++){
			console.log(
				xPos, 
				this.elementPosition.y + (parseInt(this.getEditor().fontSize) * (this.getEditor().lineHeight * i)), 
				dimensions.width
			);
	
			this.canvas.context.fillText(
				this.getEditor().rendered[i].replace("%f%", "").replace("%n%", ""),
				xPos, 
				this.elementPosition.y + (parseInt(this.getEditor().fontSize) * (this.getEditor().lineHeight * i)), 
				dimensions.width
			);
        }
	
		this.canvas.context.fillStyle = fill;
        this.canvas.context.font = font;
	};

	this.drawOutlines = function(dimensions){
	    let size = 2;
	    let lineWidth = 2;
		let xs = this.elementPosition.x - lineWidth;
	    let ys = this.elementPosition.y - lineWidth;

		let lines = (this.getEditor().totalLines == 0) ? 1 : this.getEditor().totalLines;
		if(this.getEditor().rendered[this.getEditor().rendered.length - 1] == ''){
			if(!this.getEditor().canEdit()){
				lines--;
			}
		}

		let width = dimensions.width;
		let height = (this.getEditor().lineHeight * parseInt(this.getEditor().fontSize) * lines);

		this.canvas.context.lineWidth =  lineWidth;
		this.canvas.context.fillStyle = '#2381e0';
		this.canvas.context.strokeStyle = '#2381e0';

		if(this.isWritable()){
			ys -= parseInt(this.getEditor().fontSize);
		}

		this.canvas.context.beginPath();
			if(this.isResizable()){
				this.canvas.context.arc(xs, ys, size, 0, 2 * Math.PI);
				this.canvas.context.fill();
			}
			
			this.canvas.context.moveTo(xs, ys);
			this.canvas.context.lineTo(xs + width, ys);
			this.canvas.context.stroke();
		this.canvas.context.closePath();

		this.canvas.context.beginPath();
			if(this.isResizable()){
				this.canvas.context.arc(xs + width, ys, size, 0, 2 * Math.PI);
				this.canvas.context.fill();
			}

			this.canvas.context.moveTo(xs + width, ys);
			this.canvas.context.lineTo(xs + width, ys + height);
			this.canvas.context.stroke();
		this.canvas.context.closePath();


		this.canvas.context.beginPath();
			if(this.isResizable()){
				this.canvas.context.arc(xs + width, ys + height, size, 0, 2 * Math.PI);
				this.canvas.context.fill();
			}

			this.canvas.context.moveTo(xs + width, ys + height);
			this.canvas.context.lineTo(xs, ys + height);
			this.canvas.context.stroke();
		this.canvas.context.closePath();


		this.canvas.context.beginPath();
			if(this.isResizable()){
				this.canvas.context.arc(xs, ys + height, size, 0, 2 * Math.PI);
				this.canvas.context.fill();
			}

			this.canvas.context.moveTo(xs, ys + height);
			this.canvas.context.lineTo(xs, ys);
			this.canvas.context.stroke();
		this.canvas.context.closePath();
	};

	if (!(this instanceof Canvas_TextElement)) { throw('Constructor called without "new"'); }
	return this.init.apply(this, [canvas, options]);
}
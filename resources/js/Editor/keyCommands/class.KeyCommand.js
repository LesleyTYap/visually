function KeyCommand(config){
    
    this.name;
	this.keyCode;
	this.ctrl;
	this.alt;
	this.shift;
	this.command;
	
	this.init = function(config){
		if(typeof config.name == 'undefined' || typeof config.keyCode == 'undefined' || typeof config.command == 'undefined'){
			throw('Not all required options are set: ' + config.name);
		}
		this.name = config.name;
		this.keyCode = config.keyCode;
		this.command = config.command;

		this.ctrl = (typeof config.ctrl != 'undefined') ? config.ctrl : false;
		this.alt = (typeof config.alt != 'undefined') ? config.alt : false;
		this.shift = (typeof config.shift != 'undefined') ? config.shift : false;

		return this;
	};
	if (!(this instanceof KeyCommand)) { throw('Constructor called without "new"'); }
	return this.init.apply(this, [config]);
}
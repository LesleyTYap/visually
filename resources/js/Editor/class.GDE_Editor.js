function GDE_Editor(config){
    
    this.content;
    this.rendered;
    this.lines;
    
    this.position;
    this.totalLines;
    this.currentLine;
	this.maxLine;
    
    this.width;
    this.height;
    this.paddding;
    
    this.font;
    this.fontSize;
    this.lineHeight;
	this.color = null;

    this.outputAdaptor;
    this.selection;
    this.utility;

    this.active;
	
	this.init = function(config){
		if(typeof config.content == 'undefined'){
            config.content = '';
        }
		if(typeof config.width == 'undefined'){
            config.width = 500;
        }
		if(typeof config.height == 'undefined'){
            config.height = 500;
        }
		if(typeof config.fontSize == 'undefined'){
            config.fontSize = 16;
        }
		if(typeof config.lineHeight == 'undefined'){
            config.lineHeight = 1.5;
        }
		if(typeof config.maxLine == 'undefined'){
            config.maxLine = null;
        }
		if(typeof config.color == 'undefined'){
            config.color = '#000000';
        }
        if(typeof config.padding == 'undefined'){
            config.padding = {
                left: 10,
                top: 12,
                right: 10,
                bottom: 12,
            };
        }
		if(typeof config.font == 'undefined'){
            config.font = 'Open Sans';
        }
		if(typeof config.textAlign == 'undefined'){
            config.textAlign = 'left';
        }
        if(typeof config.outputAdaptor == 'undefined'){
            throw('An adaptor must be implemented"');
        } else {
            if(typeof config.outputAdaptor.process != 'function'){
               throw('An OutputAdaptor must implement the "process" method');
            }
            if(typeof config.outputAdaptor.moveCursor != 'function'){
                throw('An OutputAdaptor must implement the "moveCursor" method');
            }  
        }
        
        this.content = config.content;
        this.outputAdaptor = config.outputAdaptor;
        this.width = config.width;
        this.height = config.height;

        this.fontSize = config.fontSize;
        this.font = config.font;
        this.color = config.color;
        this.textAlign = config.textAlign;
        
        this.lineHeight = config.lineHeight;
        this.padding = config.padding;
        
        this.lines = 0;
        this.maxLine = config.maxLine;
        this.position = { relative : 0, absolute : 0, stored : null };
        this.totalLines = 0;
        this.currentLine = 0;
        this.rendered = [];

        this.selection = new Selection(this);
        this.utility = new GDE_EditorUtilities();

        this.actions = new GDE_EditorActions();

        this.active = false;
        
		return this;
	};
	if (!(this instanceof GDE_Editor)) { throw('Constructor called without "new"'); }
	return this.init.apply(this, [config]);
}

GDE_Editor.prototype.enable = function(){
    this.active = true;
}

GDE_Editor.prototype.disable = function(){
    this.active = false;
    this.clearSelected();
    this.outputAdaptor.canvasElement.state = 0;
    this.outputAdaptor.moveCursor(this, this.currentLine, null);
    this.outputAdaptor.process();
}

GDE_Editor.prototype.isActive = function(){
    return this.active;
}

GDE_Editor.prototype.processDefault = function(event){
    if(event.ctrlKey || event.altKey){ return; }

    if(this.acceptedCharacters.indexOf(event.key) != -1){
        if(this.hasSelection()){
            this.removeSelection();
            this.clearSelected();
            this.utility.fitTextIntoEditor(this);
        }
        if(!this.canEdit()){ return; }

        var modifier = (this.content.substring(this.position.absolute, this.position.absolute + 3) == '%n%') ? 3 : 0;    
        this.content = this.content.substring(
            0,
            this.position.absolute,
        ) + 
        event.key +
        this.content.substring(
            this.position.absolute,
        );
        
        this.addToPosition(event.key.length + modifier);
        if(modifier > 0){
            this.currentLine++;
            this.setPosition({ relative : 1 });
        }

        this.utility.fitTextIntoEditor(this);
        this.outputAdaptor.process(this);
                
        if(this.content.substr(this.position.absolute, this.position.absolute + 1) == '%n%'){
            if(this.canEdit(true)){ 
                this.currentLine++;
                this.setPosition({ relative : 0 });
                this.addToPosition({ absolute : 3 });
            }
        }

        this.updateCursor();
        this.refreshPoint();
        this.clearSelected();
    }
};

GDE_Editor.prototype.getInnerHeight = function(lookForward){
    let lines = this.totalLines == 0 ? this.totalLines + 1 : this.totalLines;
    if(lookForward === true){ lines++; }
    return parseInt(this.fontSize) * parseFloat(this.lineHeight) * lines;
}

GDE_Editor.prototype.getContentHeight = function(){
    let lines = this.totalLines == 0 ? this.totalLines + 1 : this.totalLines;
    if(this.height < this.getInnerHeight()){ lines--; }
    return parseInt(this.fontSize) * parseFloat(this.lineHeight) * lines;
};

GDE_Editor.prototype.canEdit = function(lookForward){
    if(!this.isActive()){ return false; }
    var innerHeight = this.getInnerHeight(lookForward);
    if(innerHeight <= this.height){
        if(innerHeight == this.height){
            return false;
        }
        return true;
    } return false;
};

GDE_Editor.prototype.clearSelected = function(){
    this.selection.clearSelection();
    this.outputAdaptor.selected({});
};

GDE_Editor.prototype.updateCursor = function(){
    this.outputAdaptor.moveCursor(
        this, this.currentLine, 
        this.utility.getStringWidth(
            this, 
            this.getCurrentLine().substring(0, this.position.relative)
        )
    );
}

GDE_Editor.prototype.updateOutput = function(event){
    this.outputAdaptor.process(this);
    this.updateCursor();
};

GDE_Editor.prototype.refreshPoint = function(){
    this.selection.point = {
        line : this.currentLine,
        position : this.position
    }
};

GDE_Editor.prototype.addToPosition = function(value) {
    if(typeof value == 'number'){
        this.position.relative += value;
        this.position.absolute += value
    } else {
        if(typeof value.relative != 'undefined'){
            this.position.relative += value.relative;
        }
        if(typeof value.absolute != 'undefined'){
            this.position.absolute += value.absolute;
        }
    }
}

GDE_Editor.prototype.subtractFromPosition = function(value) {
    if(typeof value == 'number'){
        this.position.relative -= value;
        this.position.absolute -= value
    } else {
        if(typeof value.relative != 'undefined'){
            this.position.relative -= value.relative;
        }
        if(typeof value.absolute != 'undefined'){
            this.position.absolute -= value.absolute;
        }
    }
}

GDE_Editor.prototype.setPosition = function(value) {
    if(typeof value.relative != 'undefined'){
        this.position.relative = value.relative;
    }
    if(typeof value.absolute != 'undefined'){
        this.position.absolute = value.absolute;
    }
    if(typeof value.stored != 'undefined'){
        this.position.stored = value.stored;
    }
}

GDE_Editor.prototype.storeCurrentPosition = function(){
    this.setPosition({ stored : this.position.relative });
}

GDE_Editor.prototype.clearCurrentPosition = function(){
    this.setPosition({ stored : null });
}

GDE_Editor.prototype.adjustForStoredPosition = function(){
    if(this.position.stored <= this.getCurrentLine().length){
        if(this.position.relative < this.position.stored){
            this.addToPosition(this.position.stored - this.position.relative);
        } else {
            this.subtractFromPosition(this.position.relative - this.position.stored);
        }
    } else if(this.position.stored > this.getCurrentLine().length) {
        if(this.position.relative < this.getCurrentLine().length){
            this.addToPosition(this.getCurrentLine().length - this.position.relative);
        }
    }
}

GDE_Editor.prototype.getCurrentLine = function(){
    return (typeof this.rendered[this.currentLine] == 'undefined') ? '' : this.rendered[this.currentLine];
};

GDE_Editor.prototype.removeAtNextPosition = function(){
    var test = this.content.substring(this.position.absolute, this.position.absolute + 3);
    if(test == '%f%'){
        this.content = this.content.substring(
            0,
            this.position.absolute,
        ) + 
        this.content.substring(
            this.position.absolute + 3,
            this.content.length
        );
    } else if(test == '%n%'){
        var remove;
        test = this.content.substring(this.position.absolute - 3, this.position.absolute);
        if(test == '%f%'){
            remove = 3;
            this.subtractFromPosition({ absolute : 3 });
        } else {
            test = this.content.substring(this.position.absolute + 3, this.position.absolute + 6);
            if(test == '%n%' || test == '%f%'){
                remove = 6;
            } else { remove = 4; }
        }

        this.content = this.content.substring(
            0,
            this.position.absolute,
        ) + 
        this.content.substring(
            this.position.absolute + remove,
            this.content.length
        );
    } else {
        this.content = this.content.substring(
            0,
            this.position.absolute,
        ) + 
        this.content.substring(
            this.position.absolute + 1,
            this.content.length
        );
    }
};

GDE_Editor.prototype.removeAtPreviousPosition = function(){
    var move;
    var test = this.content.substring(this.position.absolute, this.position.absolute - 3);

    if(test == '%n%' || test == '%f%'){
        this.content = this.content.substring(
            0,
            this.position.absolute - 3,
        ) + 
        this.content.substring(
            this.position.absolute,
            this.content.length
        );
        move = { relative : 1, absolute : 3 };
    } else {
        this.content = this.content.substring(
            0,
            this.position.absolute - 1,
        ) + 
        this.content.substring(
            this.position.absolute,
            this.content.length
        );
        move = { relative : 1, absolute : 1 };
    }
    this.subtractFromPosition(move);

    if(this.position.relative < 0 && this.currentLine > 0){
        this.currentLine--;
        this.position.relative = this.getCurrentLine().length;
    } else if(this.position.relative < 0) {
        this.position.relative = 0;
    }
    if(this.position.absolute < 0){
        this.position.absolute = 0
    }
};

GDE_Editor.prototype.removeSelection = function(){
    var selection = this.selection.getSelection(this); 
    var content = this.content.substring(selection.start.position.absolute, selection.end.position.absolute);
    content = content.replace(/(?:%n%|%f%)/g, ' ');

    var event = (selection.origin.position.absolute < selection.end.position.absolute) ? -1 : 1;
    for(var i = 0; i < content.length; i++){
        if(event === -1){
            this.removeAtPreviousPosition();
        } else if(event === 1) {
            this.removeAtNextPosition();
        }
    }

    return content;
}

GDE_Editor.prototype.hasSelection = function(){
    return this.selection.hasSelection();
};

GDE_Editor.prototype.acceptedCharacters = [" ", "!", "", "#", "$", "%", "&", "'", "(", ")", "*", "+", ",", "-", ".", "/", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", ":", ";", "<", "=", ">", "?", "@", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "[", "\\", "]", "^", "_", "`", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "{", "|", "}", "~"];

GDE_Editor.prototype.utility;
function CommandList(){
        
    this.context;
    this.eventHandler;

    this.commandList = {};
    this.commandPairings;

	this.init = function(){	
        this.commandPairings = new Dictionary();
        this.eventHandler = this.processEvent.bind(this);

		return this;
	};
	if (!(this instanceof CommandList)) { throw('Constructor called without "new"'); }
	return this.init.apply(this, []);
}

CommandList.prototype.addCommand = function(command){
    this.addPairing(command.keyCode, command.name);
    this.commandList[command.keyCode] = command;
    return true;
}

CommandList.prototype.remmoveCommand = function(id){
    var command =  this.getCommand(id);
    if(typeof command !== false){
        this.removePairing(command.keyCode);
        delete this.commandList[command.keyCode];
        return true;
    } return false;
}

CommandList.prototype.getCommand = function(identifier){
	if(typeof this.commandList[identifier] != 'undefined'){
        return this.commandList[identifier];
    } else {
        var key = this.commandPairings.getKey(identifier);
        if(key !== false && this.commandList[key] != 'undefined'){
            return this.commandList[key];
        }
    }
    return false;
}

CommandList.prototype.commandExists = function(identifier){
	return this.getCommand(identifier);
}

CommandList.prototype.runCommand = function(identifier, event){
    var key = this.commandPairings.get(identifier);
    if(typeof  this.commandList[key].command === 'function'){
        if(this.getContext().isActive()){
            return this.commandList[key].command.call(this, this.getContext(), this.commandList[key], event);
        }
    } return false;
}

CommandList.prototype.processEvent = function(event){
    if (event.defaultPrevented) { return; }
    
    var key, handled = false;
    if (event.key !== undefined) {
        key = event.key;
        handled = true;
    } else if (event.keyCode !== undefined) {
        key = event.keyCode;
        handled = true;
    }

    if(event.altKey){
        key = 'a-'+key;
    }
    if(event.shiftKey){
        key = 's-'+key;
    }
    if(event.ctrlKey){
        key = 'c-'+key;
    }
    
    if(this.commandExists(key)){
        if (handled) { event.preventDefault(); }
        this.runCommand(key);
    } else {
        this.getContext().processDefault(event);
    }
}

CommandList.prototype.setContext = function(context) {
    this.context = context;
}

CommandList.prototype.getContext = function() {
    return this.context;
}

CommandList.prototype.addPairing = function(keyCode, name) {
    this.commandPairings.add(keyCode, name);
}

CommandList.prototype.removedPairing = function(keyCode) {
    this.commandPairings.remove(keyCode);
}
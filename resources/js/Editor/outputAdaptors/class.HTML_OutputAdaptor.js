function HTML_OutputAdaptor(element){	
    
    this.element;
    this.cursorElement;
    this.selectedElement;
    this.elementStyles;
    
    this.init = function(element){    
        this.element = element;
        this.elementStyles = window.getComputedStyle(this.element);
		return this;
    };
    
    this.process = function(editor){
        output = editor.content;
        output = output.replace(/(?:%f%)/g, '<br>');
        output = output.replace(/(?:%n%)/g, '<br>');
        this.element.innerHTML = output;
    };

    this.moveCursor = function(editor, line, x){
        if(typeof this.cursorElement == 'undefined'){
            this.cursorElement = document.createElement('span');
            this.cursorElement.classList.add('gde-cursor');

            this.cursorElement.style.top = '0px';
            this.cursorElement.style.left = '0px';
            
            this.element.parentNode.appendChild(this.cursorElement);
        }
        
        this.cursorElement.style.left = x + parseInt(this.elementStyles.getPropertyValue('padding-left')) + 'px';
        this.cursorElement.style.top = (line * (editor.fontSize * editor.lineHeight)) + parseInt(this.elementStyles.getPropertyValue('padding-top')) + 'px';
    };

    this.selected = function(areas){
        var span;
        if(typeof this.selectedElement == 'undefined'){
            this.selectedElement = document.createElement('div');
            this.selectedElement.classList.add('gde-selected');
            this.element.parentNode.appendChild(this.selectedElement);
        }
        this.selectedElement.innerHTML = '';
        for(var index in areas){
            if(areas.hasOwnProperty(index)){
                span = document.createElement('span');
        
                span.style.top = ((index) * (editor.fontSize * editor.lineHeight)) + parseInt(this.elementStyles.getPropertyValue('padding-top')) + 'px';
                span.style.left = areas[index].offset + parseInt(this.elementStyles.getPropertyValue('padding-left')) + 'px';
                span.style.width = areas[index].width + 'px';
                    
                this.selectedElement.appendChild(span);
            }
        }
    }


	if (!(this instanceof HTML_OutputAdaptor)) { throw('Constructor called without "new"'); }
	return this.init.apply(this, [element]);
}
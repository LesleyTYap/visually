GDE_EditorActions.prototype.delete = function(context, command) { 
    event.stopPropagation();
    if(context.hasSelection()){
        context.removeSelection();
        context.clearSelected();
    } else {
        context.removeAtNextPosition();
    }

    context.outputAdaptor.cursorElement.classList.add('static');
    context.utility.fitTextIntoEditor(context);
    context.updateOutput.call(context);
    context.outputAdaptor.cursorElement.classList.remove('static');

    context.refreshPoint();
};
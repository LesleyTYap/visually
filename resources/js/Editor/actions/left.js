GDE_EditorActions.prototype.left = function(context, command) {     
    event.stopPropagation();
    if(context.selection.area != null){
        context.selection.clearSelection(context);
        return;
    }

    context.clearCurrentPosition();
    
    if(context.position.absolute > 0){	
        if(context.position.relative == 0 && context.currentLine != 0){
            context.currentLine--;
            context.setPosition({ relative : context.getCurrentLine().length + 1 });
        }
        var move;
        var test = context.content.substring(context.position.absolute - 3, context.position.absolute);
        if(test == '%n%' || test == '%f%'){
            move = { relative : 1, absolute : 3 };
        } else { move = { relative : 1, absolute : 1 }; }
    
        context.outputAdaptor.cursorElement.classList.add('static');
        context.subtractFromPosition(move);
        context.updateOutput.call(context);
        context.outputAdaptor.cursorElement.classList.remove('static');
        
        context.refreshPoint();
    }
};
GDE_EditorActions.prototype.up = function(context, command) { 		
    event.stopPropagation();
    if(context.selection.area != null){
        context.selection.clearSelection(context);
        return;
    }

    if(context.currentLine > 0){
        var position = context.position.relative;
        if(context.position.stored == null){
            context.storeCurrentPosition();
        }

        
        context.currentLine--;
        if(context.position.relative > context.getCurrentLine().length){						
            context.subtractFromPosition({ absolute : 
                (
                    context.position.relative + 
                    3
                )
            });
            context.setPosition({ relative : context.getCurrentLine().length });
        } else {
            context.subtractFromPosition({ absolute : 
                (
                    context.position.relative + 
                    context.getCurrentLine().substring(context.position.relative, context.getCurrentLine().length).length + 
                    3
                )
            });
            context.setPosition({ relative : context.position.relative});
        }
        context.adjustForStoredPosition();

        context.outputAdaptor.cursorElement.classList.add('static');
        context.updateOutput.call(context);
        context.outputAdaptor.cursorElement.classList.remove('static');

        context.refreshPoint();
    }
};
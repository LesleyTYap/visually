GDE_EditorActions.prototype.home = function(context, command) { 	
    event.stopPropagation();
    if(context.selection.area != null){
        context.selection.clearSelection(context);
        return;
    }

    var start = {
        line : context.currentLine, 
        position : {
            relative : 0,
            absolute : context.position.absolute - context.position.relative,
            stored : null
        }
    };

    context.setPosition({
        relative : start.position.relative,
        absolute : start.position.absolute
    });

    context.updateCursor();
};
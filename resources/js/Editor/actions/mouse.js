GDE_EditorActions.prototype.mouse = {};

GDE_EditorActions.prototype.mouse.clickCounter = { left: 0, right: 0 };
GDE_EditorActions.prototype.mouse.addClickCount = function(context, type){
    if(this.clickTimer){
        clearTimeout(this.clickTimer);
    }

    if(type === 'left'){
        this.clickCounter.left++;
    } if(type === 'right'){
        this.clickCounter.right++;
    }
    this.determineClick(context, type); 

    this.clickTimer = setTimeout(function(context, type){ console.log('reset'); this.resetClickCount(type); }.bind(this, context, type), 400);
};
GDE_EditorActions.prototype.mouse.resetClickCount = function(type){
    if(type === 'left' || typeof type == 'undefined'){
        this.clickCounter.left = 0;
    } if(type === 'right' || typeof type == 'undefined'){
        this.clickCounter.right = 0;
    }
};
GDE_EditorActions.prototype.mouse.clickTimer = null;

GDE_EditorActions.prototype.mouse.down = function(context, command, event) {
    let canMouseX = parseInt(event.clientX - context.outputAdaptor.canvasElement.canvasOffset.left);
    let canMouseY = parseInt(event.clientY - context.outputAdaptor.canvasElement.canvasOffset.top);
    
    if(!context.outputAdaptor.canvasElement.isTargetable(canMouseX, canMouseY)){ return true; }

    // event.stopPropagation();
    if(context.content.length > 0){
        context.selection.clearSelection(context);
        context.selection.select();
        context.selection.getPoint(context);
        context.updateOutput.call(context);
        context.actions.mouse.addClickCount.call(context.actions.mouse, context, 'left');
    }
};

GDE_EditorActions.prototype.mouse.determineClick = function(context, type) {
    if(type == 'left'){
        if(context.actions.mouse.clickCounter.left == 2){
            context.selection.selectWord(context);
        } else if(context.actions.mouse.clickCounter.left >= 3){
            context.selection.selectSentence(context);
        }
    }
}

GDE_EditorActions.prototype.mouse.up = function(context, command, event) {  
    let canMouseX = parseInt(event.clientX - context.outputAdaptor.canvasElement.canvasOffset.left);
    let canMouseY = parseInt(event.clientY - context.outputAdaptor.canvasElement.canvasOffset.top);
    
    if(!context.outputAdaptor.canvasElement.isTargetable(canMouseX, canMouseY)){ return true; }
    // event.stopPropagation();
    context.selection.blur();
    if(context.selection.isDragging){        
        context.updateOutput.call(context);
    }
};

GDE_EditorActions.prototype.mouse.move = function(context, command, event) { 
    let canMouseX = parseInt(event.clientX - context.outputAdaptor.canvasElement.canvasOffset.left);
    let canMouseY = parseInt(event.clientY - context.outputAdaptor.canvasElement.canvasOffset.top);
    
    if(!context.outputAdaptor.canvasElement.isTargetable(canMouseX, canMouseY)){ return true; }
    // event.stopPropagation();
    if(context.selection.isSelecting && event.buttons == 1){
        context.selection.isDragging = true;
        context.selection.fetchSelection(context);
    } else {

    }
};
GDE_EditorActions.prototype.select = {};

GDE_EditorActions.prototype.select.all = function(context, command, event) { 
    if(!context.isActive()){ return false; }

    context.selection.selectAll(context);
    // context.selection.fetchSelection(context);

    if((context.totalLines - 1) == context.currentLine && context.height < context.getInnerHeight()){
        context.currentLine--;
        context.subtractFromPosition({absolute : 3});
        context.setPosition({relative : context.rendered[context.currentLine].length});
        context.updateCursor();
    }

    context.updateOutput.call(context);
};
GDE_EditorActions.prototype.enter = function(context, command) {
    event.stopPropagation();
    if(!context.canEdit(true)){ return; }
    if(context.hasSelection()){
        context.removeSelection();
        context.clearSelected();
    }

    context.content = context.content.substring(
        0,
        context.position.absolute,
    ) + 
    '%f%' +
    context.content.substring(
        context.position.absolute,
    );

    context.updateOutput.call(context);
    context.currentLine++;
    context.addToPosition({ absolute : 3 });
    context.setPosition({ relative : 0 });
    context.utility.fitTextIntoEditor(context);
    context.updateOutput.call(context);

    context.refreshPoint();
};
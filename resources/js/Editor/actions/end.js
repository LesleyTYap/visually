GDE_EditorActions.prototype.end = function(context, command) { 	
    event.stopPropagation();
    if(context.selection.area != null){
        context.selection.clearSelection(context);
        return;
    }

    var line = context.getCurrentLine();
    var end = {
        line : context.currentLine, 
        position : {
            relative : line.length,
            absolute : context.position.absolute + (line.length - context.position.relative),
            stored : null
        }  
    };

    context.setPosition({
        relative : end.position.relative,
        absolute : end.position.absolute
    });

    context.updateCursor();
};
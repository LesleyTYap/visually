GDE_EditorActions.prototype.right = function(context, command) { 	
    event.stopPropagation();
    if(context.selection.area != null){
        context.selection.clearSelection(context);
        return;
    }

    context.clearCurrentPosition();

    if(context.height < context.getInnerHeight() && context.currentLine + 1 >= context.rendered.length - 1){ 
        if(context.position.relative == context.getCurrentLine().length){
            return; 
        }
    }

    if(context.content.length >= context.position.absolute + 1){
        if(context.position.relative == context.getCurrentLine().length && context.currentLine < context.totalLines){
            context.currentLine++;
            context.setPosition({ relative : -1 });
        }
        var move;
        var test = context.content.substring(context.position.absolute, context.position.absolute + 3);
        if(test == '%n%' || test == '%f%'){
            move = { relative : 1, absolute : 3 };
        } else { move = { relative : 1, absolute : 1 }; }

    
        context.outputAdaptor.cursorElement.classList.add('static');
        context.addToPosition(move);
        context.updateOutput.call(context);
        context.outputAdaptor.cursorElement.classList.remove('static');
        
        context.refreshPoint();
    }
};
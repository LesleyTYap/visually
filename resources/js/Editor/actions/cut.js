GDE_EditorActions.prototype.cut = function(context, command) { 			
    event.stopPropagation();
    var content = context.removeSelection();
    context.utility.copy(content);

    context.clearSelected();
    context.selection.clearSelection(context);
    
    context.updateOutput.call(context);
    context.refreshPoint();
};
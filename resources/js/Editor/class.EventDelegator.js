function EventDelegator(commandList){
    
	this.commandList;
	this.delegateEvent;
	
	this.init = function(commandList){
		if(typeof commandList == 'undefined'){
			throw('Not all required parameters are set');
		}
		this.commandList = commandList;
		this.delegateEvent = this.delegate.bind(this);

		return this;
	};
	if (!(this instanceof EventDelegator)) { throw('Constructor called without "new"'); }
	return this.init.apply(this, [commandList]);
}

EventDelegator.prototype.addDelegator = function(element){
	if(typeof element == 'undefined'){ throw('elements must be a HTMLElements or an array containing HTMLElements.'); }
	if(this.isElement(element)){
		element = [element];
	}
	if(typeof element.length != 'undefined'){
		for(var i = 0; i < element.length; i++){
			element[i].addEventListener('mouseup', this.delegateEvent, false);
			element[i].addEventListener('mousedown', this.delegateEvent, false);
			element[i].addEventListener('mousemove', this.delegateEvent, false);
		}
	}
};

EventDelegator.prototype.removeDelegator = function(element){
	if(typeof element == 'undefined'){ throw('elements must be a HTMLElements or an array containing HTMLElements.'); }
	if(this.isElement(element)){
		element = [element];
	}
	
	if(typeof element.length != 'undefined'){
		for(var i = 0; i < element.length; i++){
			element[i].removeEventListener('click', this.delegateEvent, false);
			element[i].removeEventListener('mouseup', this.delegateEvent, false);
			element[i].removeEventListener('mousedown', this.delegateEvent, false);
			element[i].removeEventListener('mousemove', this.delegateEvent, false);
		}
	}
};

EventDelegator.prototype.delegate = function(event){
	//check if editor is active
	if(event.target.classList.contains('gde-active')){
		if(event.type == 'mousedown'){
			this.commandList.runCommand('LeftMouseDown', event);
		} else if(event.type == 'mouseup'){
			this.commandList.runCommand('LeftMouseUp', event);
		} else if(event.type == 'mousemove'){
			this.commandList.runCommand('MouseMove', event);
		}
	} else {
		if(event.type == 'mousedown'){
			event.target.classList.add('gde-active');
			window.addEventListener('keydown', this.commandList.eventHandler, false);
		}
		
		if(event.type == 'mouseup'){
			var callback = function(e){
				e.stopPropagation();
				event.target.classList.remove('gde-active');
				window.removeEventListener('keydown', this.commandList.eventHandler, false); 
				window.removeEventListener('mousedown', callback, false);
			}.bind(this);
			window.addEventListener('mousedown', callback, false);
		}
	}
}

EventDelegator.prototype.isElement = function(o){
	return (
	  	typeof HTMLElement === "object" ? o instanceof HTMLElement : //DOM2
		o && typeof o === "object" && o !== null && o.nodeType === 1 && typeof o.nodeName==="string"
  	);
  }
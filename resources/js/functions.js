// De anitmatie van de button te activeren wanneer er op geklikt word.
var button = document.querySelector('.bt-menu-trigger');
button.addEventListener('click', function (){
    button.classList.toggle('bt-menu-open');
});

// Het animerend openen van de navigatie
$('.bt-menu-trigger').click(function () {
    var x = document.getElementById("overlay");
    if (x.style['margin-left'] === "0px"){                              // Als de overlay styling inhoud, een margin left van 0px
        $('#overlay').animate({"margin-left": '500px'});      // Dan animeer de overlay naar een margin left van 220px
    } else {                                                            // Anders
        $('#overlay').animate({"margin-left": '0px'});        // Animeer de overlay naar margin left van 0px
    }
});

// Het openen van het preview menu door op het pijltje te klikken.
$('.left_button').click(function(){
    var x = document.querySelector(".preview_menu");
    if (x.style['margin-left'] === "0px") {                                                                     // Als de preview_menu styling inhoud, een margin left van 0px
        if($('.main_container').find('#expand').length) {                                                       // Als hij een expand_field kan vinden (er een open is)
            var container = document.getElementsByClassName("preview_container");                    // Sla het element preview_container op in var container
            var buttons = container[0].getElementsByClassName("expand_button");                      // Sla alle buttons in de preview_container op in var buttons
            for (var i=0; i<buttons.length; i++){                                                               // Herhaal voor elke button alles tussen deze brackets
                if($(buttons[i]).find(".expand_image").hasClass("expand_image_active")){                        // Als de betreffende button de class active heeft
                    $(buttons[i]).css("display", "");                                                           // Dan voeg css toe display ""
                    $(buttons[i]).find(".expand_image").removeClass("expand_image_active");                     // Vind de betreffende button en verwijder de active class
                    $('.expand_field').animate({"margin-left": '-=910px'}, function(){       // Animeer het expand_field met een margin left -910px met wat het al had
                        $('#expand').remove();                                                                  // En verwijder dan het expand_field
                        $('.preview_menu').animate({"margin-left": '-=265px'});                       // Animeer dan de preview_menu margin left -265px dan wat hij had.
                    });

                }
            }
        }
    } else {                                                                                                    // Anders
        $('.preview_menu').animate({"margin-left": '+=265px'});                                       // Animeer het preview_menu met een margin left +250px met wat het al had.
    }
    $('.open_arrow').toggleClass('close_arrow');                                                                // Wanneer er op de 'left_button' geklikt word activeer de rotatie animtatie
});

// Zorgt ervoor dat de button op hover tevoorschijnkomt wanneer de button niet actief is en het menu nog niet is uitgeklapt en laat de button niet zien wanneer een button actief is.
$('.hover_field').hover(
    function(){
        if(!$(this).find('.expand_image').hasClass('expand_image_active')){
            $(this).find('.expand_button').css('display', 'inline');
        }
    },
    function() {
        if(!$(this).find('.expand_image').hasClass('expand_image_active')){
            $(this).find('.expand_button').css('display', '');
        }
    });

// Wanneer er op de button word geklikt

    //      (if)     Als er geen expand_field open is
    //               Vind de offset van het hover_field
    //               Voeg 'code' toe
    //               Voeg daar de styling aan toe
    //               Vind expand_image (pijl in button)
    //               Voeg daar de class aan toe met een transform in de styling zodat hij 180deg draait
    //               Animeer expand_field naar volledige width; 660px

    //      (else)   Als er wel een expand_field open is

    //              (forloop)   Herhaal voor elke button alles tussen deze brackets;

    //                          (if)    Als deze button active is,
    //                                  maak de button weer onzichtbaar, draai de button pijl terug

    //                                  (if)    Als de active button gelijk is aan de desbetreffende button: this
    //                                          Animeer expand_field naar 0 en verwijder de expand.
    //
    //                          (else)
    //                                  (if)    Als de active button gelijk is aan de desbetreffende button: this
    //                                          Dan voeg class active toe
    //                                          Sla de offset van het hover_field op in var hoverposition
    //                                          Animeer expand_field naar 0
    //                                              Verwijder de expand
    //                                              Voeg code toe
    //                                              Voeg daar de styling/locatie aan toe
    //                                              Animeer expand naar volledige breedte, 660px
    //

$('.expand_button').click(
    function(){
        var code = '<div class="expand_field" id="expand">' +
        '<div class="scrollable_expand scrollbar">' +
        '<div class="expand_preview">' +
        '<div class="expand_container"></div>' +
        '<div class="titel">' +
        '<h2>Preview</h2>' +
        '</div>' +
        '</div>' +
        '<div class="expand_preview">' +
        '<div class="expand_container"></div>' +
        '<div class="titel">' +
        '<h2>Preview</h2>' +
        '</div>' +
        '</div>' +
        '<div class="expand_preview">' +
        '<div class="expand_container"></div>' +
        '<div class="titel">' +
        '<h2>Preview</h2>' +
        '</div>' +
        '</div>' +
        '<div class="expand_preview">' +
        '<div class="expand_container"></div>' +
        '<div class="titel">' +
        '<h2>Preview</h2>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '</div>';
        if(!$('.main_container').find('#expand').length){
            var hoverposition = $(this).parent().parent().offset();
            $('.main_container').prepend(code);
            $('.main_container').find('.expand_field').css('top', hoverposition.top-20);
            $(this).find(".expand_image").addClass("expand_image_active");
            $('.expand_field').animate({"margin-left": '+=660px'});
        }
        else{
            var container = document.getElementsByClassName("preview_container");
            var buttons = container[0].getElementsByClassName("expand_button");
              for (var i=0; i<buttons.length; i++){
                if($(buttons[i]).find(".expand_image").hasClass("expand_image_active")){
                    $(buttons[i]).css("display", "");
                    $(buttons[i]).find(".expand_image").removeClass("expand_image_active");
                    if(buttons[i] == this){
                        $('.expand_field').animate({"margin-left": '-=660px'}, function(){
                            $('#expand').remove();
                        });
                    }
                }
                else{
                    if(buttons[i] == this){
                        $(this).find(".expand_image").addClass("expand_image_active");
                        var hoverposition = $(this).parent().parent().offset();
                        $('.expand_field').animate({"margin-left": '-=660px'}, function(){
                            $('#expand').remove();
                            $('.main_container').prepend(code);
                            $('.main_container').find('.expand_field').css('top', hoverposition.top-20);
                            $('.expand_field').animate({"margin-left": '+=660px'}, function(){

                            });
                        });
                    }
                }
            }
        }
    });

$('.preview_menu').scroll(function(){
    var container = document.getElementsByClassName("preview_container");                  // Sla het element preview_container op in var container
    var buttons = container[0].getElementsByClassName("expand_button");                   // Sla alle buttons in de preview_container op in var buttons
    for (i = 0; i < buttons.length; i++) {                                                           // Herhaal voor elke button alles tussen deze brackets
        if ($(buttons[i]).find(".expand_image").hasClass("expand_image_active")) {                  // Als deze button active is
            var activepos = $(buttons[i]).parent().parent().offset();                              // Sla locatie van deze button op
            $('.expand_field').css('top', activepos.top - 20);                                    // Verplaats expand_field naar de nieuwe plek
        }
    }
});



$('.afmeting_button').click(function(){
    var container = document.getElementsByClassName("afmeting_button_con");              // Sla container om de buttons op in container
    var buttons = container[0].getElementsByClassName("afmeting_button");               // Sla alle buttons in de container op in buttons
    for (var i=0; i<buttons.length; i++){                                                          // Herhaal voor elke button alles tussen de brackets
        if ($(buttons[i]).hasClass("afmeting_button_active")){                                    // Als de betreffende button de active class heeft
            $(buttons[i]).removeClass("afmeting_button_active");                                 // Dan verwijder de active class
        } else {                                                                                // Anders
            if(buttons[i] == this){                                                            // Als de active button gelijk is aan de desbetreffende button: this
                $(buttons[i]).addClass("afmeting_button_active");                             // Dan voeg de active class aan de betreffende button toe
            }

        }
    }

});


(function(window){
	
	var visually;
	var GDEChecker = document.querySelectorAll('.responsive-checker')[0];

	var commands, deligator, editor, adaptor;
	// setTimeout(function(){
	// 	var editors = document.querySelectorAll('.gde-editor-inner');
	// 	for(var i = 0; i < editors.length; i++){
	// 		initEditor(editors[i]);
	// 	}
		
	// }, 0);
	initWorkspace();
	
})(window);



function initWorkspace(){
	inherits(Canvas_TextElement, Canvas_Element);
	inherits(Canvas_ImageElement, Canvas_Element);

	setTimeout(function(){
		let header = document.querySelector('.header_container');
		let workspace = document.querySelector('.vy-workspace');
		let inner = document.querySelector('.vy-workspace-inner');
		let canvas = document.querySelector('#vy-workspace-canvas');

		visually = new GDE_Workspace(canvas, '2d');
		visually.setDimensions(400, 400);
		visually.getCanvas().setPadding(80);

		visually.tools.format.loader.load(visually, 'Basic Example 2');

		let sizes = [
			{width:1200, height:800},
			{width:1200, height:300},
			{width:400, height:400},
		];

		let nav = document.createElement('div');
		nav.classList.add('vy-workspace-navigation');
		
		let label = document.createElement('label');
		label.classList.add('vy-upload-label');
		label.classList.add('label_as_button');
		label.setAttribute('for', 'vy-workspace-upload');
		label.innerHTML = 'Upload';
		nav.appendChild(label);

		for(let i = 0; i < sizes.length; i++){
			let button = document.createElement('button');
			button.classList.add('vy-format-picker');
			button.classList.add('afmeting_button');
			button.type = 'button';
			button.innerHTML = sizes[i].width + ' x ' + sizes[i].height;
			button.onclick = visually.setDimensions.bind(visually, sizes[i].width, sizes[i].height);
			nav.appendChild(button);
		}

		header.appendChild(nav);
		//workspace.insertBefore(nav, inner);
	}, 0);
};



function initEditor(element){
	adaptor = new Canvas_OutputAdaptor(element);
	editor = new GDE_Editor({
		'content': '',
		'outputAdaptor' : adaptor,
		'width' : 400,
		'height' : 400
	});
}
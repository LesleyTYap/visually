function WorkspaceStyle_Default(options){
    this.edgeLength;
    
    this.init = function(options){ 
        if(typeof options == 'undefined'){ options = {}; }
        if(typeof options.edgeLength == 'undefined'){
            options.edgeLength = 15;
        }    
        this.edgeLength = options.edgeLength;
        return this; 
    };
    
    if (!(this instanceof WorkspaceStyle_Default)) { throw('Constructor called without "new"'); }
    return this.init.apply(this, [options]);
}


WorkspaceStyle_Default.prototype.createOutline = function(context){	
	context.getCanvas().context.globalCompositeOperation = 'destination-over';
	context.getCanvas().context.fillStyle = 'rgba(208, 208, 208, 0.7)';
	context.getCanvas().context.fillRect(0, 0, context.getCanvas().element.width, context.getCanvas().element.height);
	
	let inner = { width : context.getCanvas().canvasInnerWidth(), height : context.getCanvas().canvasInnerHeight() };
	context.getCanvas().context.globalCompositeOperation = 'source-over';
	context.getCanvas().context.fillStyle = 'transparent';
	
	context.getCanvas().context.clearRect(
		(context.getCanvas().canvasWidth() - context.workspace.size.width ) / 2, 
		(context.getCanvas().canvasHeight() - context.workspace.size.height ) / 2, 
		context.workspace.size.width, 
		context.workspace.size.height
		);

	let lineWidth = 1;
	let xs = context.offsetLeft();
	let ys = context.offsetTop();
	let width = context.workspace.size.width + (lineWidth * 2);
	let height = context.workspace.size.height + (lineWidth);

	context.getCanvas().context.lineWidth =  lineWidth;
	context.getCanvas().context.strokeStyle = '#2381e0';

	context.getCanvas().context.setLineDash([10, 4]);/*dashes are 5px and spaces are 3px*/
	context.getCanvas().context.beginPath();
	context.getCanvas().context.moveTo(xs, ys);
	context.getCanvas().context.lineTo(xs + width, ys);

	context.getCanvas().context.moveTo(xs + width, ys);
	context.getCanvas().context.lineTo(xs + width, ys + height);

	context.getCanvas().context.moveTo(xs + width, ys + height);
	context.getCanvas().context.lineTo(xs, ys + height);

	context.getCanvas().context.moveTo(xs, ys + height);
	context.getCanvas().context.lineTo(xs, ys);

	context.getCanvas().context.moveTo(xs, ys);
	context.getCanvas().context.closePath();

	context.getCanvas().context.stroke();

	context.getCanvas().context.setLineDash([]);

	context.getCanvas().context.globalCompositeOperation = 'destination-over';
};



WorkspaceStyle_Default.prototype.addEdges = function(context){
	context.getCanvas().context.globalCompositeOperation = 'source-over';
	let outer = { width : context.getCanvas().canvasWidth(), height : context.getCanvas().canvasHeight() };
	let inner = { width : context.getDimensions('width'), height : context.getDimensions('height') };
	
	let start = -10;
	let divider = 2;
	let distance = Math.abs(start) + 7;
	let size = this.edgeLength * 2;

	context.getCanvas().context.strokeStyle = '#2381e0';
	
	/* left top marking */
	context.getCanvas().context.beginPath();
	context.getCanvas().context.lineWidth = 2;
	context.getCanvas().context.moveTo(start -1 + ((outer.width - inner.width) / divider), start + ((outer.height - inner.height) / divider));
	context.getCanvas().context.lineTo(start + this.edgeLength + ((outer.width - inner.width) / divider), start + ((outer.height - inner.height) / divider));
	context.getCanvas().context.moveTo(start + ((outer.width - inner.width) / divider), start + ((outer.height - inner.height) / divider));
	context.getCanvas().context.lineTo(start + ((outer.width - inner.width) / divider), start + this.edgeLength + ((outer.height - inner.height) / divider));
	context.getCanvas().context.stroke();
		/* small mark*/
		context.getCanvas().context.beginPath();
		context.getCanvas().context.lineWidth = 1;
		context.getCanvas().context.moveTo(-1-distance + ((outer.width - inner.width) / divider), -distance + ((outer.height - inner.height) / divider));
		context.getCanvas().context.lineTo(-distance + size + ((outer.width - inner.width) / divider), -distance + ((outer.height - inner.height) / divider));
		context.getCanvas().context.moveTo(-distance + ((outer.width - inner.width) / divider), -distance + ((outer.height - inner.height) / divider));
		context.getCanvas().context.lineTo(-distance + ((outer.width - inner.width) / divider), -distance + size + ((outer.height - inner.height) / divider));
		context.getCanvas().context.stroke();

	/* left bottom marking */
	context.getCanvas().context.beginPath();
	context.getCanvas().context.lineWidth = 2;
	context.getCanvas().context.moveTo(start + ((outer.width - inner.width) / divider), -start + outer.height - ((outer.height - inner.height) / divider));
	context.getCanvas().context.lineTo(start + ((outer.width - inner.width) / divider), -start + outer.height - this.edgeLength - ((outer.height - inner.height) / divider) );
	context.getCanvas().context.moveTo(start-1 + ((outer.width - inner.width) / divider), -start + outer.height - ((outer.height - inner.height) / divider));
	context.getCanvas().context.lineTo(start + this.edgeLength + ((outer.width - inner.width) / divider), -start + outer.height - + ((outer.height - inner.height) / divider));
	context.getCanvas().context.stroke();
		/* small mark*/
		context.getCanvas().context.beginPath();
		context.getCanvas().context.lineWidth = 1;
		context.getCanvas().context.moveTo(-distance + ((outer.width - inner.width) / divider), distance + outer.height - ((outer.height - inner.height) / divider));
		context.getCanvas().context.lineTo(-distance + ((outer.width - inner.width) / divider), distance + outer.height - size - ((outer.height - inner.height) / divider) );
		context.getCanvas().context.moveTo(-1-distance + ((outer.width - inner.width) / divider), distance + outer.height - ((outer.height - inner.height) / divider));
		context.getCanvas().context.lineTo(-distance + size + ((outer.width - inner.width) / divider), distance + outer.height - + ((outer.height - inner.height) / divider));
		context.getCanvas().context.stroke();

	/* right top marking */
	context.getCanvas().context.beginPath();
	context.getCanvas().context.lineWidth = 2;
	context.getCanvas().context.moveTo(-start + 1 + outer.width - ((outer.width - inner.width) / divider), start + ((outer.height - inner.height) / divider));
	context.getCanvas().context.lineTo(-start + outer.width - this.edgeLength - ((outer.width - inner.width) / divider), start + ((outer.height - inner.height) / divider));
	context.getCanvas().context.moveTo(-start + outer.width - ((outer.width - inner.width) / divider), start + ((outer.height - inner.height) / divider));
	context.getCanvas().context.lineTo(-start + outer.width - ((outer.width - inner.width) / divider), start + this.edgeLength + ((outer.height - inner.height) / divider) );
	context.getCanvas().context.stroke();
		/* small mark*/
		context.getCanvas().context.beginPath();
		context.getCanvas().context.lineWidth = 1;
		context.getCanvas().context.moveTo(distance + 1 + outer.width - ((outer.width - inner.width) / divider), -distance + ((outer.height - inner.height) / divider));
		context.getCanvas().context.lineTo(distance + outer.width - size - ((outer.width - inner.width) / divider), -distance + ((outer.height - inner.height) / divider));
		context.getCanvas().context.moveTo(distance + outer.width - ((outer.width - inner.width) / divider), -distance + ((outer.height - inner.height) / divider));
		context.getCanvas().context.lineTo(distance + outer.width - ((outer.width - inner.width) / divider), -distance + size + ((outer.height - inner.height) / divider) );
		context.getCanvas().context.stroke();

	/* right bottom marking */
	context.getCanvas().context.beginPath();
	context.getCanvas().context.lineWidth = 2;
	context.getCanvas().context.moveTo(-start + 1 + outer.width - ((outer.width - inner.width) / divider), -start + outer.height - ((outer.height - inner.height) / divider));
	context.getCanvas().context.lineTo(-start + outer.width - this.edgeLength - ((outer.width - inner.width) / divider), -start + outer.height - ((outer.height - inner.height) / divider));
	context.getCanvas().context.moveTo(-start + outer.width - ((outer.width - inner.width) / divider), -start + outer.height - ((outer.height - inner.height) / divider));
	context.getCanvas().context.lineTo(-start + outer.width - ((outer.width - inner.width) / divider), -start + outer.height - this.edgeLength - ((outer.height - inner.height) / divider));
	context.getCanvas().context.stroke();
		/* small mark*/
		context.getCanvas().context.beginPath();
		context.getCanvas().context.lineWidth = 1;
		context.getCanvas().context.moveTo(distance + 1 + outer.width - ((outer.width - inner.width) / divider), distance + outer.height - ((outer.height - inner.height) / divider));
		context.getCanvas().context.lineTo(distance + outer.width - size - ((outer.width - inner.width) / divider), distance + outer.height - ((outer.height - inner.height) / divider));
		context.getCanvas().context.moveTo(distance + outer.width - ((outer.width - inner.width) / divider), distance + outer.height - ((outer.height - inner.height) / divider));
		context.getCanvas().context.lineTo(distance + outer.width - ((outer.width - inner.width) / divider), distance + outer.height - size - ((outer.height - inner.height) / divider));
		context.getCanvas().context.stroke();

	context.getCanvas().context.globalCompositeOperation = 'destination-over';
};

WorkspaceStyle_Default.prototype.draw = function(context){
    this.addEdges(context);
    this.createOutline(context);
};
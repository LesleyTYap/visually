/* 
	- Check why draw function isn't triggered when switching to variant
	- Make positions & size work percentage based


	MINIMUM FUNCTIONALITIES (ALPHA)
	- Have format selection working
	- Add font-size silder to influence fontsize of given textfield
	- Download image
	- Select social channel formats (Having canvas resize to actually fit / display correctly)
		- Resize format to fit social channel size
	- Have a zoom in on canvas functionality
	- Add support for background colors
		- Add format settings to allocate colors to specific elements
	- Add toolbar
	- Create account page
	- Create editor page


	(BETA)
	- Add variations support on formats
	- Save image / dashboard for created visuals
	- Add payment support
	- Add login system
	- Add multi image upload support in formats

	FINAL PRODUCT
	- Add social campagin manager (premium)
	- Create custom template (premium)
	- Allow to create GIFS (free/premium)
	- Request custom format from HellVisual
	- White labeled (premium)
*/

function GDE_Workspace(element, context, options){
	
	this.tools = {};
	this.workspace = {};

	this.init = function(element, context, options){
		this.tools = {
			element : {
				builder :new GDE_ElementBuilder()
			},
			file : {
				loader : new GDE_FileLoader() // set callback
			},
			format : {
				loader : new GDE_FormatLoader({ callback : this.formatLoaded.bind(this) })
			},
			canvas : {
				element : new GDE_Canvas(element, context),
				renderer : new GDE_CanvasRenderer()
			},
			cursor : new GDE_Cursor({ callback: this.handleMouse.bind(this) })
		};

		if(typeof options == 'undefined'){ options = {}; }
		if(typeof options.style == 'undefined'){
			options.style = new WorkspaceStyle_Default({edgeLength : 15});
		}

		this.workspace = {
			size : { },
			style : options.style
		}
		this.workspace.size.width = this.getCanvas().element.width;
		this.workspace.size.height = this.getCanvas().element.height;

		this.handleEvent = this.handleEvent.bind(this);
		this.getCanvas().element.addEventListener('mousedown', this.handleEvent, false);
		this.getCanvas().element.addEventListener('mousewheel', this.handleEvent, false);
		this.getCanvas().element.addEventListener('DOMMouseScroll', this.handleEvent, false);

		this.build();

		return this;
	};

	if (!(this instanceof GDE_Workspace)) { throw('Constructor called without "new"'); }
	return this.init.apply(this, [element, context, options]);
}


GDE_Workspace.prototype.getCanvas = function(){
	return this.tools.canvas.element;
}

GDE_Workspace.prototype.getCursor = function(){
	return this.tools.cursor;
}

GDE_Workspace.prototype.getElementBuilder = function(){
	return this.tools.element.builder;
}

GDE_Workspace.prototype.getFileLoader = function(){
	return this.tools.file.loader;
}

GDE_Workspace.prototype.getFormatLoader = function(){
	return this.tools.format.loader;
}

GDE_Workspace.prototype.getCanvasRenderer = function(){
	return this.tools.canvas.renderer;
}

GDE_Workspace.prototype.getWorkplaceStyle = function(){
	return this.workspace.style;
}

GDE_Workspace.prototype.setDimensions = function(width, height){
	this.workspace.size.width = width;
	this.workspace.size.height = height;
	
	if(this.getFormatLoader().format){
		console.log(this.getFormatLoader().format);
		// this.getFormatLoader().set(this, this.getFormatLoader().format);
	}

	this.draw();
};

GDE_Workspace.prototype.getDimensions = function(property){
	if(typeof this.workspace.size[property] !== 'undefined'){
		return this.workspace.size[property];
	} return this.workspace.size;
};

GDE_Workspace.prototype.offsetLeft = function(){
	return ((this.getCanvas().canvasWidth() - this.workspace.size.width ) / 2) - 1;
};

GDE_Workspace.prototype.offsetTop = function(){
	return ((this.getCanvas().canvasHeight() - this.workspace.size.height ) / 2) - 1;
};





















/* MOVE THIS STUFF SOMEWHERE ELSE */






GDE_Workspace.prototype.handleEvent = function(e){
	var elements = this.getFormatLoader().elements;
	var target = this.getCursor().determineTarget(this, elements, e);
	if(target !== false){
		target.doEvent(e, e.type);
	}
}


GDE_Workspace.prototype.handleMouse = function(){
	var elements = this.getFormatLoader().elementsInOrder();
	var cursor = this.getCursor().determinePointer(elements);
	this.getCanvas().element.style.cursor = cursor;
}







GDE_Workspace.prototype.build = function(){
	let upload = this.getElementBuilder().build({ type : 'input', options : { type : 'file', className : 'sf-canvas-uploader' } });
	upload.id = 'vy-workspace-upload';
	upload.addEventListener('change', this.load.bind(this), false);
	this.getCanvas().element.parentNode.insertBefore(upload, this.getCanvas().element);
};

GDE_Workspace.prototype.load = function(e){
	var src = false;
	for(var index in this.getFormatLoader().elements){
		if(this.getFormatLoader().elements.hasOwnProperty(index)){
			if(this.getFormatLoader().elements[index] instanceof Canvas_Element){
				if(this.getFormatLoader().elements[index] == 'background'){
					src = this.getFormatLoader().elements[index].getImageData();
				}
				// this.localEvents.deregister(this.getFormatLoader().elements['background'], 'background');
				// delete this.getFormatLoader().elements['background'];
			}
		}
	}

	

	let options = {
		type : 'image',
		name : 'background',
		resizable : true,
		draggable : true,
		order : 1,
	};

	if(src != false){
		options.src = src;
	}

	this.getFileLoader().importImage(e, this.loaded.bind(this, options));
};

GDE_Workspace.prototype.loaded = function(options, image){
	this.getCanvasRenderer().enable();
	options.src = image;
	options.imageLoaded = this.draw.bind(this);

	let hasBackground = false;
	for(var index in this.getFormatLoader().elements){
		this.getFormatLoader().elements[index].clearActive();
		if(this.getFormatLoader().elements[index].getName() === 'background'){
			hasBackground = true;
		}
	}
	
	if(hasBackground){
		delete this.getFormatLoader().elements[index];
	} else { index++; }
	this.getFormatLoader().elements[index] = new Canvas_ImageElement(this.getCanvas(), options);
	this.getFormatLoader().elements[index].setEvents(this);
	
	this.getFormatLoader().ordered = null;
	this.getFormatLoader().elementsInOrder();



};


GDE_Workspace.prototype.formatLoaded = function(total){
	this.getCanvasRenderer().enable();
	this.buildFormatMenu();
	this.draw();
};


GDE_Workspace.prototype.buildFormatMenu = function(){
	if(this.getFormatLoader().format === null || typeof this.getFormatLoader().format.variaty === 'undefined' || this.getFormatLoader().format.variaty.length <= 0){ return; }

	let menu = this.getCanvas().element.parentNode.querySelector('.sf-variaty-menu');
	if(menu === null){
		menu = this.getElementBuilder().build(
			{ type : 'div', options : { className : 'sf-variaty-menu' } }
		);
		this.getCanvas().element.parentNode.insertBefore(menu, this.getCanvas().element);
	} else { menu.innerHTML = ''; }
	
	for(let i = 0; i < this.getFormatLoader().format.variaty.length; i++){
		let button = this.getElementBuilder().build(
			{ type : 'button', options : { type : 'button', className : 'sf-variaty-menu-item' } }
		);
		button.innerHTML = this.getFormatLoader().format.variaty[i];
		button.onclick = function(context, name){ this.load(context, name); }.bind(this.getFormatLoader(), this, this.getFormatLoader().format.variaty[i]);
		menu.appendChild(button);
	}
};


GDE_Workspace.prototype.draw = function(){
	this.getCanvasRenderer().clear(this);
	this.getWorkplaceStyle().draw(this);
	this.getFormatLoader().draw();
}